import { CREATE_USER, LOGIN, LOGOUT, UPDATE_USER } from './actionTypes'

const createUser = (user) => {
    return {
        type: CREATE_USER,
        user
    }
}

const loginUser = (user) => {
    return {
        type: LOGIN,
        user,
    }
}

const logoutUser = () => {
    return {
        type: LOGOUT
    }
}


const updateUser = (user) => {
    return {
      type: UPDATE_USER,
      user
    }
  }
    

export {createUser, loginUser, logoutUser, updateUser}