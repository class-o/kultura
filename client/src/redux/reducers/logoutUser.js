import {
    LOGOUT
} from "../actionTypes";

const initialState = {};

const BACKEND_URL = "http://localhost:5000";

const logoutReducer = (state = initialState, action) => {
    console.log(action);
    switch (action.type) {
        case LOGOUT: {
            fetch(`${BACKEND_URL}/user/logout`).then((response) => {
                if (response.status === 200) {
                    console.log('logout')
                }
            })
            localStorage.removeItem('user')
            console.log('LOGOUT')
            return {
                user: undefined
            };
        }
        default: {
            return state;
        }
    }
};

export default logoutReducer;