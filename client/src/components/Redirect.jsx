import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import "./redirect.css";
import { createUser } from "../redux/actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

class Redirect extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    const userId = this.props.match.params.id;
    console.log(userId);
    this.props.dispatch({
      id: userId,
      userType: "user",
    });
    localStorage.setItem(
      "user",
      JSON.stringify({
        user: userId,
        userType: "user",
        ...{
          authdata: window.btoa(`"fbandgmekipa":"123456"`),
        },
      })
    );
    this.props.history.push("/");
  }
  render() {
    return (
      <div>
        <span>setting you up ...</span>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (user) => dispatch(createUser(user)),
  };
};
export default withRouter(connect(null, mapDispatchToProps)(Redirect));
