import * as React from 'react';
import { BsGeoAlt } from "react-icons/bs";

const pinStyle = {
  fill: '#d00',
  stroke: 'none',
  width: "50px ",
  height: "50px"
};

function Pin(props) {
  const { size = 20 } = props;

  return (
    <BsGeoAlt height={size} viewBox="0 0 24 24" style={pinStyle}></BsGeoAlt>
  );
}

export default React.memo(Pin);