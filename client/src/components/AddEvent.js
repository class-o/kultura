import React, { useEffect } from "react";
import TextField from '@material-ui/core/TextField';
import { authHeader } from "./../helpers/authHeader";
import { ToastContainer, toast } from "react-toastify";
import { Marker } from "react-map-gl";
import icon from "../data/output-onlinepngtools.png";
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import "./addEvent.css";
import { BACKEND_URL } from "../helpers/constaints";

class AddEvent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: "",
      start: "",
      end: "",
      tags: "",
      people: "",
      description: "",
      id_type: 0,
      visible: this.props.visible,
      errors: {
        errors: [],
      },
      category: "",
      writtenLocation: ""
    }
    this.handleChange = this.handleChange.bind(this);
    this.onChange = this.onChange.bind(this);
    this.escFunction = this.escFunction.bind(this);
    this.cancel = this.cancel.bind(this);
  }


  handleChange() {
    var isChecked = document.getElementById("switch").checked;
    if (isChecked) isChecked = 0;
    else isChecked = 1;
    this.setState({
      ...this.state,
      id_type: isChecked === "" || isChecked === false ? 0 : 1,
    });
  }

  addEvent() {
    if (this.props.userLocation.latitude != null) {
      <Marker
        key={"user"}
        latitude={this.props.userLocation.latitude}
        longitude={this.props.userLocation.longitude}
      >
        <button
          style={{ backgroundColor: "transparent", width: "20px", border: "none" }}
          onClick={e => {
          }}
        >
          <img src={icon} style={{ width: "30px" }} />
        </button>
      </Marker>
    }
  };


  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      ...this.state,
      [name]: value,
    });
  };

  checkDate = () => {
    if (this.state.start >= this.state.end) {
      toast.error("Uneseni termin je neispravan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        zIndex: 1,
      });
      return true;
    }
    return false;
  }

  find(array, key) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] !== undefined) {
        return array[i];
      }
    }
    return null;
  }

  cancel = () => {
    this.setState({
      ...this.state,
      visible: false,
    })
    this.props.onVisibleChange(false)
  }

  submit = (e) => {
    e.preventDefault();
    const fromData = this.state;
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const dataEvent = {
      title: fromData.title,
      description: fromData.description,
      start_datetime: fromData.start,
      end_datetime: fromData.end,
      category: 1,
      id_type: fromData.id_type,
      lat: this.props.userLocation.latitude,
      long: this.props.userLocation.longitude,
      attendance: fromData.people,
      id_organizer: this.props.id
    };
    var errors = [];

    if (this.state.title === "") {
      errors.push("title");
    }

    if (this.state.start === "") {
      errors.push("start");
    }

    if (this.state.end === "") {
      errors.push("end");
    }

    if (this.state.people === "") {
      errors.push("people");
    }

    const { error } = this.state.errors;
    this.setState({
      error: {
        ...error,
        errors: errors,
      },
    });

    if (errors.length > 0) {
      toast.error("Uneseni podatci nisu potpuni!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        zIndex: 1,
      });
      return false;
    } else {
      const options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: authHeader(),
        },
        body: JSON.stringify(dataEvent),
      }
      console.log(options.body)
      fetch(BACKEND_URL + `/events/add-event`, options).then((response) => {
        if (response.status !== 200) {
          toast.error("❌  Neispravan unos!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        } else {
          response.json().then((response) => {
          });
          toast.success("Događaj dodan!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        }
        this.props.addNewEvent();
        this.cancel();
      });
    }
  }
  escFunction(event) {
    if (event.keyCode === 27) {
      this.setState({
        ...this.state,
        visible: false,
      });
    }
  }
  componentDidMount() {
    document.addEventListener("keydown", this.escFunction, false);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this.escFunction, false);
  }

  render() {
    return (
      <div className={this.state.visible ? "visibleForm modal-dialog" : "hiddenForm modal-dialog"} role="document" style={{ top: "5.5vh", zIndex: "10 !important" }}>
        <div className="modal-content" >
          <div className="modal-header">
            <TextField
              fullWidth
              required type="text" value={this.state.title} name="title" id="recipient-name" label="Naziv događaja" onChange={this.onChange}
              className={
                this.find(this.state.errors, "title") !== null
                  ? "title is-invalid" : "title"
              }
            />
          </div>
          <div className="modal-body" style={{ height: "40vh" }}>
            <form>
              <div className="form-group" style={{ display: "flex", justifyContent: "space-around" }}>
                <TextField
                  required
                  id="datetime-local"
                  name="start"
                  value={this.state.start}
                  label="Početak događaja"
                  type="datetime-local"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChange}
                  className={
                    this.find(this.state.errors, "start") !== null
                      ? "is-invalid"
                      : ""
                  }
                />
                <TextField
                  required
                  id="datetime-local"
                  name="end"
                  value={this.state.end}
                  label="Kraj događaja"
                  type="datetime-local"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChange}
                  onBlur={this.checkDate}
                  className={
                    this.find(this.state.errors, "end") !== null
                      ? "is-invalid"
                      : ""
                  }
                />
              </div>
              <div className="form-group">
                <TextField required type="number" min="1" name="people" value={this.state.people} id="guest-number" label="Broj ljudi" onChange={this.onChange}
                  className={
                    this.find(this.state.errors, "people") !== null
                      ? "is-invalid"
                      : ""
                  }
                />
              </div>
              <FormControlLabel
                value="start"
                control={<Switch id="switch" inputProps={{ role: 'switch' }} color="primary" onChange={this.handleChange} />}
                label="Privatno:"
                labelPlacement="start"
              />
              <div className="form-group">
                <textarea className="form-control" name="description" value={this.state.description} id="message-text" placeholder="Dodaj opis" onChange={this.onChange} />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button type="cancel" className="btn btn-secondary" data-dismiss="modal" style={{ color: "rgba(196, 196, 196, 1)" }} onClick={this.cancel}>Close</button>
            <button type="submit" className="btn btn-primary" style={{ color: "rgba(196, 196, 196, 1)" }} onClick={this.submit} >Dodaj</button>
          </div>
        </div>
        <ToastContainer></ToastContainer>
      </div>

    )
  }
}

export default AddEvent;