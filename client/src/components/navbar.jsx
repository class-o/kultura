import React, { useEffect } from "react";
import { Nav } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";
import { logoutUser } from "../redux/actions";
import "./nav.css";
import Dropdown from "react-bootstrap/Dropdown";
import { BsFillPersonFill } from "react-icons/bs";
import Search from "./Search";

function Navigation(props) {
  const dispatch = useDispatch();
  const loggedUser = useSelector((state) => state.user.logged);
  const userType = useSelector((state) => state.user.userType);

  useEffect(() => {
    const burger = document.getElementById("burger");
    const nav = document.getElementById("nav-links");
    const navLinks = document.querySelectorAll(".nav-links li");
    const body = document.body;
    console.log(props);
  });

  return (
    <nav className="navbar">
      <a className="navbar-brand" style={{ color: "white" }} href="/">
        <strong>
          <span className="red-title">KUL</span>
          <span clasName="rest">TURA</span>
        </strong>
      </a>
      <div className="navSearch">
        <Search />
      </div>
      <Dropdown>
        <Dropdown.Toggle
          variant="dark"
          id="dropdown-basic"
          className="navigation-button-smaller"
        >
          <BsFillPersonFill />
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {loggedUser ? (
            <Dropdown.Item
              onClick={() => {}}
              href={userType === "user" ? `/user/${props.id}` : "/adminProfile"}
            >
              Moj Profil
            </Dropdown.Item>
          ) : (
            <Dropdown.Item href="/registration">Registracija</Dropdown.Item>
          )}
          {loggedUser ? (
            <Dropdown.Item href="/home" onClick={() => props.dispatch()}>
              Odjava
            </Dropdown.Item>
          ) : (
            <Dropdown.Item href="/login">Prijava</Dropdown.Item>
          )}{" "}
        </Dropdown.Menu>
      </Dropdown>
    </nav>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    logged: state.user.logged,
    id: state.user.id,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: () => dispatch(logoutUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
