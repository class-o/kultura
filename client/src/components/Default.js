import React from "react";
import "react-toastify/dist/ReactToastify.css";
import "./login.css";
import { GrUnlink }  from "react-icons/gr";

function Default (props) {
    
    return (
        <div className="full_height_section">
            <br />
            <br />
            <br />
            <br />
            <br />
            <div className="main-default d-flex justify-content-center " >
                <GrUnlink size={"30vh"} 
                style={{color: 'white !important' }}
                onClick={() => props.history.push("/")}/>
            </div>

            <br />
            <br />

            <div className="main-default d-flex justify-content-center"> 
                <h2>
                Oh no! Stranica nije dostupna!
                </h2>
            </div>

            <br />
            <br />
      </div>

    );
  }



export default Default;
