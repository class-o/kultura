import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { updateUser } from "../../redux/actions";
import { authHeader } from "../../helpers/authHeader";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BiFemaleSign, BiMaleSign } from "react-icons/bi";
// import ImageUploader from "react-images-upload";

import { BACKEND_URL } from "../../helpers/constaints";

class EditUserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      original_username: "",
      original_email: "",
      user: {
        id_user: "",
        first_name: "",
        last_name: "",
        username: "",
        email: "",
        picture: "",
        gender: "",
        birthday: "",
        phone_number: "",
        file: null,
      },
      errors: {
        errors: [],
      },
    };
  }

  form = React.createRef();
  onChange = (event) => {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value,
      },
    });
  };
  // onDrop = (picture) => {
  //   const { user } = this.state;
  //   this.setState({
  //     user: {
  //       ...user,
  //       file: picture.target.files[0],
  //     },
  //   });
  //   console.log(this.state.user.file);
  // };

  usernameCheck = (event) => {
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    fetch(
      BACKEND_URL +
        `/user/username-available/?username=${this.state.user.username}`,
      options
    ).then((response) => {
      if (response.status === 400) {
        toast.error("❌ Korisničko ime nije slobodno!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
  };
  emailCheck = (event) => {
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    fetch(
      BACKEND_URL + `/user/email-available/?email=${this.state.user.email}`,
      options
    )
      .then((response) => {
        if (response.status === 400) {
          toast.error("❌ Email se već koristi!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return false;
        }
      })
      .catch(console.log("tu"));
    const expression = /\S+@\S+/;
    var checkAt = expression.test(String(this.state.user.email).toLowerCase());
    console.log(checkAt);
    if (
      this.state.user.email !== "" &&
      this.state.user.email.len != 0 &&
      !checkAt
    ) {
      toast.error("❌ Email treba sadržavati znak @!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return false;
    }

    return true;
  };

  find(array, key) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] !== undefined) {
        return array[i];
      }
    }
    return null;
  }

  onSubmit = (e) => {
    e.preventDefault();

    console.log(this.state);

    var errors = [];

    if (this.state.user.username === "") {
      errors.push("username");
    }

    if (this.state.user.first_name === "") {
      errors.push("first_name");
    }

    if (this.state.user.last_name === "") {
      errors.push("last_name");
    }

    if (
      this.state.user.email !== this.state.original_email &&
      !this.emailCheck()
    ) {
      errors.push("email");
    }

    const { error } = this.state.errors;
    this.setState({
      error: {
        ...error,
        errors: errors,
      },
    });
    console.log(errors.length);
    if (errors.length > 0) {
      return false;
    }
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(this.state.user),
    };
    console.log(options);
    console.log(this.state.user.file);
    fetch(BACKEND_URL + `/user/edit`, options).then((response) => {
      console.log("ok");
      if (response.status !== 200) {
        toast.error("❌ Spremanje nije moguće!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        this.props.history.push("/userprofile");
      }
    });
  };

  componentDidMount() {
    fetch(BACKEND_URL + `/user/${this.props.id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
        Authorization: authHeader(),
      },
    }).then((response) => {
      console.log(response);
      response.json().then((json) => {
        console.log("json:" + JSON.stringify(json));
        this.setState({
          original_email: json.email,
          original_username: json.email,
          user: {
            id_user: json.id_user,
            gender: json.gender,
            birthday: json.birthday,
            picture: json.picture,
            phone_number: json.phone_number,
            first_name: json.first_name,
            last_name: json.last_name,
            username: json.username,
            email: json.email,
          },
        });
        console.log(this.state.user);
      });
    });
  }

  cancel = () => {
    this.props.history.push("/userprofile");
  };

  onValuesChange = (changed, all) => {
    this.setState(changed, () => console.log(this.state));
  };

  render() {
    return (
      <div className="Login">
        {/* <div className="d-flex flex-column bg justify-content-between"> */}
        <div className="mx-auto mt-1 text-center">
          <label>
            <strong className="title-log">UREDI PROFIL</strong>
          </label>
        </div>
        <form>
          <div className="next text-center">
            <div className="form-reg center justify-content-center mx-auto">
              <div className="mx-auto text-center divnext" id="fna">
                <input
                  className={
                    this.find(this.state.errors, "first_name") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="first_name"
                  placeholder="First name"
                  value={this.state.user.first_name}
                  onChange={this.onChange}
                ></input>
              </div>
              <div className="mx-auto text-center divnext" id="lna">
                <input
                  className={
                    this.find(this.state.errors, "last_name") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="last_name"
                  placeholder="Last name"
                  onChange={this.onChange}
                  value={this.state.user.last_name}
                ></input>
              </div>
              <div className="mx-auto text-center col-sm-12 my-1" id="use">
                <input
                  className={
                    this.find(this.state.errors, "username") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="username"
                  placeholder="Username"
                  onChange={this.onChange}
                  onBlur={
                    this.state.user.username !== this.state.original_username
                      ? this.usernameCheck
                      : ""
                  }
                  value={
                    this.state.user.username == "undefined"
                      ? ""
                      : this.state.user.username
                  }
                  required
                ></input>
              </div>

              <div className="mx-auto text-center col-sm-12 my-1" id="ema">
                <input
                  className={
                    this.find(this.state.errors, "email") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="email"
                  type="email"
                  placeholder="Email"
                  onChange={this.onChange}
                  onBlur={
                    this.state.user.email !== this.state.original_email
                      ? this.emailCheck
                      : ""
                  }
                  value={this.state.user.email}
                  required
                ></input>
              </div>

              <div className="mx-auto text-center col-sm-12 my-1" id="ema">
                <input
                  className={
                    this.find(this.state.errors, "birthday") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="birthday"
                  type="date"
                  placeholder="Rođendan"
                  onChange={this.onChange}
                  value={this.state.user.birthday}
                ></input>
              </div>

              <div className="mx-auto text-center col-sm-12 my-1" id="ema">
                <input
                  className={"form-control"}
                  name="phone"
                  type="number"
                  placeholder="Broj mobitela"
                  onChange={this.onChange}
                  value={this.state.user.phone_number}
                ></input>
              </div>

              <div className="mx-auto text-center col-sm-12 my-1" id="ema">
                <input
                  className={"form-control"}
                  name="picture"
                  placeholder="URL slike profila"
                  onChange={this.onChange}
                  value={this.state.user.picture}
                ></input>
                {/* <input type="file" name="file" onChange={this.onDrop} /> */}
              </div>

              <div
                className=" col-sm-12 my-1"
                style={{ display: "flex", justifyContent: "space-around" }}
              >
                <div class="form-check">
                  <input
                    class="form-check-input"
                    onChange={this.onChange}
                    name="female"
                    value={this.state.user.gender}
                    type="radio"
                    name="gender"
                    id="female"
                  />
                  <label class="form-check-label" for="female">
                    <BiFemaleSign />
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    onChange={this.onChange}
                    value="male"
                    type="radio"
                    name="gender"
                    id="male"
                  />
                  <label class="form-check-label" for="male">
                    <BiMaleSign />
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    onChange={this.onChange}
                    value="other"
                    type="radio"
                    name="gender"
                    id="other"
                  />
                  <label class="form-check-label" for="other">
                    Other
                  </label>
                </div>
              </div>

              <button
                className="btn mt-2 text-align text-center btn-primary"
                style={{ border: "1px solid black" }}
                float="center"
                type="submit"
                disabled={this.state.errors.errors.length > 0}
                onClick={this.onSubmit}
              >
                SPREMI
              </button>
              <button
                className="btn mt-2 text-align text-center btn-secondary"
                style={{ border: "1px solid black" }}
                float="center"
                type="cancel"
                onClick={this.cancel}
              >
                ODUSTANI
              </button>
            </div>
          </div>
        </form>

        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: (data) => dispatch(updateUser(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(EditUserProfile));
