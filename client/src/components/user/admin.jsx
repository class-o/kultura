import React, { useState, useSelector } from "react";
// import "./userProfile.css";
import "./admin.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../../redux/actions";
import avatar from "./user.png";
import { FiSettings, FiMessageCircle } from "react-icons/fi";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button as ButtonA, Popconfirm } from "antd";
import moment from "moment";
import "moment/locale/eu";
import { authHeader } from "../../helpers/authHeader";
import EventComponent from "../event/eventComponent";
import { Scrollbars } from "react-custom-scrollbars";
import { Tabs, Tab, TabPanel, TabList } from "react-tabs";
// import ReservationTable from "./ReservationTable";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Button,
} from "@material-ui/core";

var momentRange = require("moment-range");
momentRange.extendMoment(moment);

import { BACKEND_URL } from "../../helpers/constaints";

class AdminProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      places: [],
      pendingPlaces: [],
      events: [],
      hangs: [],
    };
  }

  onChange = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  componentDidMount() {
    console.log(this.props);
    console.log(this.props.id);
    console.log(this.props.match.params.id);

    //this.props.id.id

    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
    };

    fetch(
      `${BACKEND_URL}/admin/users/all/${this.props.id}`,
      requestOptions
    ).then((response) => {
      response.json().then((json) => {
        this.setState({
          users: json,
        });
        console.log("users");
        console.log(json);
      });
    });
    fetch(
      `${BACKEND_URL}/admin/events/all/${this.props.id}`,
      requestOptions
    ).then((response) => {
      response.json().then((json) => {
        this.setState({
          events: json,
        });
        console.log("events");
        console.log(json);
      });
    });
    fetch(
      `${BACKEND_URL}/admin/places/all/${this.props.id}`,
      requestOptions
    ).then((response) => {
      response.json().then((json) => {
        this.setState({
          places: json,
        });
        console.log("places");
        console.log(json);
      });
    });
    fetch(
      `${BACKEND_URL}/admin/hangouts/all/${this.props.id}`,
      requestOptions
    ).then((response) => {
      response.json().then((json) => {
        this.setState({
          hangs: json,
        });
        console.log("hangouts");
        console.log(json);
      });
    });
    fetch(`${BACKEND_URL}/admin/all/non-checked`, requestOptions).then(
      (response) => {
        response.json().then((json) => {
          this.setState({
            pendingPlaces: json,
          });
          console.log("pendingPlaces");
          console.log(json);
        });
      }
    );
  }

  render() {
    return (
      <div className="container container-fix">
        <div className="profile profile-fix">
          <div className="profile-header" id="view">
            <div className="profile-header-content">
              <img className="imgProfile" src={avatar} alt="" />
              <div className="pic-username">
                <div className="icons">
                  <h4
                    style={{ marginRight: "10px" }}
                    className="font-weight-300"
                  >
                    {this.state.username !== "undefined"
                      ? this.state.username
                      : ""}
                  </h4>

                  <div className="icon">
                    {this.props.match.path == "/userprofile" ||
                    this.props.id == this.props.match.params.id ? (
                      <FiSettings
                        size={20}
                        className="btn-light my-btn"
                        onClick={() => this.props.history.push("/editProfile")}
                      ></FiSettings>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="profile-sidebar">
              <div
                className="desktop-sticky-top"
                style={{ alignSelf: "start" }}
              >
                <h4>ADMIN</h4>
              </div>
            </div>
          </div>

          <div className="profile-container" id="view2">
            <br />
            <div className="adminChoose">
              <Tabs>
                <TabList>
                  <Tab>
                    <p>KORISNICI</p>
                  </Tab>
                  <Tab>
                    <p>MJESTA</p>
                  </Tab>
                  <Tab>
                    <p>EVENTI</p>
                  </Tab>
                  <Tab>
                    <p>OKUPLJANJA</p>
                  </Tab>
                </TabList>
                {/* //KORISNICI */}
                <TabPanel>
                  <div className="panel-content">
                    <TableContainer>
                      <div style={{ margin: "5%" }}>
                        <Table aria-label="custom pagination table">
                          <TableBody>
                            {this.state.users !== undefined &&
                            this.state.users.length > 0 ? (
                              this.state.users.map((row) => (
                                <TableRow key={row.id}>
                                  <TableCell component="th" scope="row">
                                    {row.id_user}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.username}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.first_name + " " + row.last_name}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.email}
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="primary"
                                      href={`user/${row.id_user}`}
                                    >
                                      Detalji
                                    </Button>
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="secondary"
                                      onClick={async () =>
                                        fetch(
                                          `${BACKEND_URL}/admin/users/delete`,
                                          {
                                            method: "POST",
                                            headers: {
                                              "Content-Type":
                                                "application/json; charset=UTF-8",
                                            },
                                            body: JSON.stringify({
                                              id_user: this.props.id,
                                              userID: row.id_user,
                                            }),

                                            //body: JSON.stringify(this.state.initialValues),
                                          }
                                        ).then((response) => {
                                          window.location.reload(false);
                                        })
                                      }
                                    >
                                      Ukloni
                                    </Button>
                                  </TableCell>
                                </TableRow>
                              ))
                            ) : (
                              <TableCell component="th" scope="row">
                                Nema dostupnih korisnika
                              </TableCell>
                            )}
                          </TableBody>
                        </Table>
                      </div>
                    </TableContainer>
                  </div>
                </TabPanel>
                {/* //MJESTA */}
                <TabPanel>
                  <div className="panel-content">
                    <TableContainer>
                      <div style={{ margin: "5%" }}>
                        <Table aria-label="custom pagination table">
                          <TableBody>
                            {this.state.pendingPlaces !== undefined &&
                            this.state.pendingPlaces.length > 0 ? (
                              this.state.pendingPlaces.map((row) => (
                                <TableRow key={row.id}>
                                  <TableCell component="th" scope="row">
                                    {row.id_place}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.name}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.email}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.lat}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.long}
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="primary"
                                      className="greenButton"
                                      onClick={async () =>
                                        fetch(
                                          `${BACKEND_URL}/admin/update-verified/`,
                                          {
                                            method: "POST",
                                            headers: {
                                              "Content-Type":
                                                "application/json; charset=UTF-8",
                                            },
                                            body: JSON.stringify({
                                              id_user: this.props.id,
                                              id_place: row.id_place,
                                              verified: true,
                                            }),

                                            //body: JSON.stringify(this.state.initialValues),
                                          }
                                        ).then((response) => {
                                          window.location.reload(false);
                                        })
                                      }
                                    >
                                      Prihvati
                                    </Button>
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="secondary"
                                      onClick={async () =>
                                        fetch(
                                          `${BACKEND_URL}/admin/update-verified/`,
                                          {
                                            method: "POST",
                                            headers: {
                                              "Content-Type":
                                                "application/json; charset=UTF-8",
                                            },
                                            body: JSON.stringify({
                                              id_user: this.props.id,
                                              id_place: row.id_place,
                                              verified: false,
                                            }),

                                            //body: JSON.stringify(this.state.initialValues),
                                          }
                                        ).then((response) => {
                                          window.location.reload(false);
                                        })
                                      }
                                    >
                                      Odbij
                                    </Button>
                                  </TableCell>
                                </TableRow>
                              ))
                            ) : (
                              <TableCell component="th" scope="row">
                                Nema novih mjesta
                              </TableCell>
                            )}
                          </TableBody>
                          <TableBody>
                            {this.state.places !== undefined &&
                            this.state.places.length > 0 ? (
                              this.state.places.map((row) => (
                                <TableRow key={row.id_place}>
                                  <TableCell component="th" scope="row">
                                    {row.id_place}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.name}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.email}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.lat}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.long}
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="primary"
                                      href={`user/${row.id_place}`}
                                    >
                                      Detalji
                                    </Button>
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="secondary"
                                      onClick={async () =>
                                        fetch(
                                          `${BACKEND_URL}/admin/places/delete`,
                                          {
                                            method: "POST",
                                            headers: {
                                              "Content-Type":
                                                "application/json; charset=UTF-8",
                                            },
                                            body: JSON.stringify({
                                              id_user: this.props.id,
                                              userID: row.id_place,
                                            }),

                                            //body: JSON.stringify(this.state.initialValues),
                                          }
                                        ).then((response) => {
                                          window.location.reload(false);
                                        })
                                      }
                                    >
                                      Ukloni
                                    </Button>
                                  </TableCell>
                                </TableRow>
                              ))
                            ) : (
                              <TableCell component="th" scope="row">
                                Nema aktivnih mjesta
                              </TableCell>
                            )}
                          </TableBody>
                        </Table>
                      </div>
                    </TableContainer>
                  </div>
                </TabPanel>
                {/* //EVENTOVI */}
                <TabPanel>
                  <div className="panel-content">
                    <TableContainer>
                      <div style={{ margin: "5%" }}>
                        <Table aria-label="custom pagination table">
                          <TableBody>
                            {this.state.events !== undefined &&
                            this.state.events.length > 0 ? (
                              this.state.events.map((row) => (
                                <TableRow key={row.id_event}>
                                  <TableCell component="th" scope="row">
                                    {row.id_event}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.title}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.id_organizer}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.id_type == 1 ? "PRIVATNO" : "JAVNO"}
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="primary"
                                      href={`event/${row.id_event}`}
                                    >
                                      Detalji
                                    </Button>
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="secondary"
                                      onClick={async () =>
                                        fetch(
                                          `${BACKEND_URL}/admin/events/delete`,
                                          {
                                            method: "POST",
                                            headers: {
                                              "Content-Type":
                                                "application/json; charset=UTF-8",
                                            },
                                            body: JSON.stringify({
                                              id_user: this.props.id,
                                              eventID: row.id_event,
                                            }),

                                            //body: JSON.stringify(this.state.initialValues),
                                          }
                                        ).then((response) => {
                                          window.location.reload(false);
                                        })
                                      }
                                    >
                                      Ukloni
                                    </Button>
                                  </TableCell>
                                </TableRow>
                              ))
                            ) : (
                              <TableCell component="th" scope="row">
                                Nema dostupnih evenata
                              </TableCell>
                            )}
                          </TableBody>
                        </Table>
                      </div>
                    </TableContainer>
                  </div>
                </TabPanel>
                {/* //OKUPLJANJA */}
                <TabPanel>
                  <div className="panel-content">
                    <TableContainer>
                      <div style={{ margin: "5%" }}>
                        <Table aria-label="custom pagination table">
                          <TableBody>
                            {this.state.hangs !== undefined &&
                            this.state.hangs.length > 0 ? (
                              this.state.hangs.map((row) => (
                                <TableRow key={row.id_hangout}>
                                  <TableCell component="th" scope="row">
                                    {row.id_hangout}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.start_datetime}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.long}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.lat}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    {row.attendance_count}
                                  </TableCell>
                                  <TableCell
                                    style={{ width: 160 }}
                                    align="right"
                                  >
                                    <Button
                                      variant="contained"
                                      color="secondary"
                                      onClick={async () =>
                                        fetch(
                                          `${BACKEND_URL}/admin/hangouts/delete`,
                                          {
                                            method: "POST",
                                            headers: {
                                              "Content-Type":
                                                "application/json; charset=UTF-8",
                                            },
                                            body: JSON.stringify({
                                              id_user: this.props.id,
                                              hangoutID: row.id_hangout,
                                            }),

                                            //body: JSON.stringify(this.state.initialValues),
                                          }
                                        ).then((response) => {
                                          window.location.reload(false);
                                        })
                                      }
                                    >
                                      Ukloni
                                    </Button>
                                  </TableCell>
                                </TableRow>
                              ))
                            ) : (
                              <TableCell component="th" scope="row">
                                Nema dostupnih okupljanja
                              </TableCell>
                            )}
                          </TableBody>
                        </Table>
                      </div>
                    </TableContainer>
                  </div>
                </TabPanel>
              </Tabs>
            </div>
          </div>
        </div>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: () => dispatch(logoutUser()),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AdminProfile)
);
