import React, { useState, useSelector } from "react";
import "./userProfile.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../../redux/actions";
import avatar from "./user.png";
import { FiSettings, FiMessageCircle } from "react-icons/fi";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button as ButtonA, Popconfirm } from "antd";
import TableContainer from "@material-ui/core/TableContainer";
import moment from "moment";
import "moment/locale/eu";
import { authHeader } from "../../helpers/authHeader";
import EventComponent from "../event/eventComponent";
import { Scrollbars } from "react-custom-scrollbars";
import { RiArrowDownSLine } from "react-icons/ri";
var momentRange = require("moment-range");
momentRange.extendMoment(moment);

import { BACKEND_URL } from "../../helpers/constaints";
import { useRef } from "react";

class UserProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id:
        this.props.userType == "admin" ||
        this.props.id == this.props.match.params.id
          ? this.props.match.params.id
          : this.props.id,
      type: this.props.userType,
      firstName: "",
      lastName: "",
      username: "",
      email: "",
      picture: "",
      gender: "",
      birthday: "",
      phoneNumber: "",
      events: [],
      myRef: "",
    };
  }
  onChange = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  componentDidMount() {
    {
      fetch(
        BACKEND_URL +
          `/user/get-user/${
            this.props.match.params.id == undefined
              ? this.props.id
              : this.props.match.params.id
          }`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json; charset=UTF-8",
            Authorization: authHeader(),
          },
        }
      ).then((response) => {
        response.json().then((json) => {
          console.log("json:" + JSON.stringify(json));
          this.setState({
            id: json.id_user,
            gender: json.gender,
            birthday: json.birthday,
            picture: json.picture,
            phoneNumber: json.phone_number,
            firstName: json.first_name,
            lastName: json.last_name,
            username: json.username,
            email: json.email,
          });
          console.log(this.state);
        });
        return fetch(
          BACKEND_URL +
            `/events/organizer/${
              this.props.match.params.id == undefined
                ? this.props.id
                : this.props.match.params.id
            }`,
          {
            method: "GET",
            headers: {
              "Content-Type": "application/json; charset=UTF-8",
              Authorization: authHeader(),
            },
          }
        ).then((response) => {
          response.json().then((json) => {
            console.log("json:" + JSON.stringify(json));
            this.setState({
              events: json,
            });
          });
        });
      });
    }
  }

  render() {
    return (
      // <div className="container container-fix">
      <div className="profile profile-fix">
        <div className="profile-header" id="view">
          <div className="profile-header-content ">
            {this.state.picture !== null && this.state.picture !== "" ? (
              <img className="imgProfile" src={this.state.picture} />
            ) : (
              <img className="imgProfile" src={avatar} alt="" />
            )}
          </div>
          <div className="profile-sidebar">
            <div className="pic-username" style={{ marginLeft: "9vw" }}>
              <div className="icons">
                <h2
                  style={{ marginRight: "10px", marginTop: "2vh" }}
                  className="font-weight-300"
                >
                  {this.state.username !== "undefined"
                    ? this.state.username
                    : ""}
                </h2>

                <div className="icon" style={{ marginTop: "2vh" }}>
                  {this.props.match.path == "/userprofile" ||
                  this.props.id == this.props.match.params.id ? (
                    <FiSettings
                      size={20}
                      className="btn-light my-btn"
                      onClick={() => this.props.history.push("/editProfile")}
                    ></FiSettings>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
            <div className="desktop-sticky-top" style={{ alignSelf: "start" }}>
              <h4>{this.state.firstName + " " + this.state.lastName}</h4>
              <br />
              <h6 className="font-weight-300 mb-3 mt-n2">
                <br />
                <h6 className="font-weight-300 mb-3 mt-n2">
                  {this.state.email}
                </h6>
              </h6>
            </div>
          </div>
        </div>
        <br />
        <div className="profile-container" id="view2">
          <br />

          <div className="profile-content">
            <div className="row">
              <div className="col-xl-12">
                <h4 className="heading"> Organizirani događaji: </h4>

                <div className="events">
                  {this.state.events.length !== 0 ? (
                    <EventComponent
                      className="event-home"
                      modelData={this.state.events}
                    ></EventComponent>
                  ) : (
                    <div style={{ color: "white" }}>
                      {" "}
                      Još niste organizirali niti jedan party 😔{" "}
                    </div>
                  )}{" "}
                </div>
                {/* <div className="scroll" ref={this.state.myRef}></div> */}
                {/* <div className="events">
                    {this.states.events.length !== 0 ? (
                      <EventComponent
                        className="event-home"
                        modelData={this.state.events}
                      ></EventComponent>
                    ) : (
                      <div style={{ color: "white" }}>
                        {" "}
                        Još niste organizirali niti jedan party 😔{" "}
                      </div>
                    )}{" "}
                  </div> */}
                {/* <div
                    className="tab-pane fade active show"
                    role="tabpanel"
                    aria-labelledby="dogs-tab"
                  >
                    {this.state.events.length == 0 ? (
                      <div style={{ color: "white" }}>
                        {" "}
                        Još niste organizirali niti jedan party 😔{" "}
                      </div>
                    ) : (
                      <EventComponent
                        modelData={this.state.events}
                      ></EventComponent>
                    )}
                  </div> */}
              </div>
            </div>
          </div>
          {/* </div> */}
        </div>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: () => dispatch(logoutUser()),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserProfile)
);
