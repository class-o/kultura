import React, { useState, useSelector } from "react";
import "./EventPage.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../../redux/actions";
import ReactMapGL, { Marker } from "react-map-gl";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Button from "react-bootstrap/Button";
import TableContainer from "@material-ui/core/TableContainer";
import moment from "moment";
import "moment/locale/eu";
import { authHeader } from "../../helpers/authHeader";
import icon from "../../data/output-onlinepngtools.png";
import Gallery from "react-photo-gallery";
import { FiSettings } from "react-icons/fi";
import { BsPeople, BsPersonPlus } from "react-icons/bs";
import Dropdown from "react-bootstrap/Dropdown";
const TOKEN =
  "pk.eyJ1IjoiaXZham5zYWpkYmFrIiwiYSI6ImNrbGFtNXpwNDA1djkyb28xdjE3czA3azcifQ.XdUshSAvDC3DoVtpKNJr2A";

var momentRange = require("moment-range");
momentRange.extendMoment(moment);

import { BACKEND_URL } from "../../helpers/constaints";

const photos = [
  {
    src: 'https://ip.index.hr/remote/indexnew.s3.index.hr/4d410452-5353-438d-b84f-6b177228be20.jpg',
    width: 3,
    height: 2
  },
  {
    src: 'https://indexnew.s3.index.hr/12ffccc9-205a-40e3-8e37-a3aacfb0fc70.jpg?v1',
    width: 5,
    height: 3
  }
];


class EventPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      viewport: {
        latitude: 45.815399,
        longitude: 15.966568,
        zoom: 12,
        width: "50vw",
        height: "35vh",
      },
      settings: {
        dragPan: true,
        dragRotate: true,
        scrollZoom: true,
        touchZoom: true,
        touchRotate: true,
        keyboard: true,
        doubleClickZoom: true,
      },
      id: this.props.id.id, // current user
      title: "",
      description: "",
      start_datetime: "",
      end_datetime: "",
      lat: "",
      long: "",
      id_organizer: "",
      firstName_organizer: "",
      lastName_organizer: "",
      organizer: "",
      attending_count: "",
      id_type: "",
      id_event: this.props.match.params.id,
      request_status: "",
      disabled: false,

      verified: [],
      nonChecked: [],

    };
  }

  going = () => {
    console.log(this.state.id_event, this.props.id)
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    console.log("tu")
    console.log(this.props.id.id)
    fetch(
      BACKEND_URL + `/events/attend-event/${this.state.id_event}/${this.props.id.id}`,
      options
    ).then((response) => {
      console.log("USAO")
      console.log(response)
      if (response.status === 400) {
        toast.error("❌ Već ste prijavljeni za događaj!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        if (this.state.id_type == 0) {
          toast.success("Woho!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        } else {
          toast.success("Poslan zahtjev!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        }
      }
    });
    this.setState({
      attending_count: this.state.attending_count + 1,
      request_status: 1,
    });
    document.getElementById("going").classList.toggle("disabled");
    this.state.disabled = true;
  }

  approve = (user) => {
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_event: this.state.id_event,
        id_user: user.id,
        allowed: true,
      })
    };
    fetch(
      BACKEND_URL + `/events/verify-request`,
      options
    ).then((response) => {
      console.log("tu")
      if (response.status === 400) {
        toast.error("❌ Neuspjeh!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        var helper = this.state.nonChecked
        helper.remove(user)
        this.setState({
          nonChecked: helper,
        });
        var helper2 = this.state.verified
        helper2.add(user)
        this.setState({
          verified: helper2,
        });
      }
    });
  }

  remove = (user) => {
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_event: this.state.id_event,
        id_user: user.id,
        allowed: false,
      })
    };
    fetch(
      BACKEND_URL + `/events/verify-requests`,
      options
    ).then((response) => {
      console.log("tu")
      if (response.status === 400) {
        toast.error("❌ Neuspjeh!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        var helper = this.state.nonChecked
        helper.remove(user)
        this.setState({
          nonChecked: helper,
        });
      }
    });
  }


  componentDidMount() {
    {
      console.log(this.state.id)

      fetch(BACKEND_URL + `/events/${this.props.match.params.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
          Authorization: authHeader(),
        },
      }).then((response) => {
        response.json().then((json) => {
          console.log("json:" + JSON.stringify(json))
          console.log(this.props)
          this.setState({
            id: json.id_user,
            title: json.title,
            description: json.description,
            start_datetime: json.start_datetime,
            end_datetime: json.end_datetime,
            lat: json.lat,
            long: json.long,
            id_type: json.id_type,
            id_organizer: json.id_organizer,
          });
          console.log(this.state);

          fetch(BACKEND_URL + `/events/attending_count/${this.state.id_event}`, {
            method: "GET",
            headers: {
              "Content-Type": "application/json; charset=UTF-8",
              Authorization: authHeader(),
            },
          }).then((response) => {
            console.log(this.state.id_event)

            response.json().then((json) => {
              console.log("json:" + JSON.stringify(json))
              this.setState({
                attending_count: json,
              });
              console.log(this.state.attending_count);

              fetch(BACKEND_URL + `/user/get-user/${this.state.id_organizer}`, {
                method: "GET",
                headers: {
                  "Content-Type": "application/json; charset=UTF-8",
                  Authorization: authHeader(),
                },
              }).then((response) => {
                response.json().then((json) => {
                  console.log("json:" + JSON.stringify(json))
                  this.setState({
                    firstName_organizer: json.first_name,
                    lastName_organizer: json.last_name,
                  });

                  fetch(BACKEND_URL + `/events/request-status/${this.state.id_event}/${this.props.id.id}`, {
                    method: "GET",
                    headers: {
                      "Content-Type": "application/json; charset=UTF-8",
                      Authorization: authHeader(),
                    },
                  }).then((response) => {
                    console.log("OVDJE")
                    response.json().then((json) => {
                      console.log("json:" + JSON.stringify(json))
                      this.setState({
                        request_status: json,
                      });
                      console.log(this.state.request_status);

                      fetch(BACKEND_URL + `/events/verified-requests/${this.state.id_event}`, {
                        method: "GET",
                        headers: {
                          "Content-Type": "application/json; charset=UTF-8",
                          Authorization: authHeader(),
                        },
                      }).then((response) => {
                        response.json().then((json) => {
                          console.log("json:" + JSON.stringify(json))
                          this.setState({
                            verified: json,
                          });
                          console.log(this.state.verified);

                          if (this.state.id_type == 1) {
                            fetch(BACKEND_URL + `/events/non-checked-requests/${this.state.id_event}`, {
                              method: "GET",
                              headers: {
                                "Content-Type": "application/json; charset=UTF-8",
                                Authorization: authHeader(),
                              },
                            }).then((response) => {
                              response.json().then((json) => {
                                console.log("json:" + JSON.stringify(json))
                                this.setState({
                                  nonChecked: json,
                                });
                                console.log(this.state.nonChecked);
                              });
                            })
                          }
                        });
                      })
                    });
                  })

                });
              })
            })
          })
        })
      }
      )
    }
  }

  render() {
    return (
      <div className="container container-fix">
        <div className="profile profile-fix">
          <div className="gallery" id="view">
            <Gallery photos={photos} />;

          </div>
          <div>
            <div className="d-flex justify-content-center title-settings">
              <h1>
                {this.state.title}
              </h1>
              {this.props.id.id == this.state.id_organizer ?
                <FiSettings className="btn btn-light my-btn settings" onClick={() => this.props.history.push(`/editEvent/${this.state.id_event}`)} ></FiSettings>
                : ""
              }
              {this.props.id.id == this.state.id_organizer ?
                <Dropdown>
                  <Dropdown.Toggle
                    variant="dark"
                    id="dropdown-basic"
                    className="navigation-button-smaller"
                  >
                    <BsPeople> </BsPeople>
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    {
                      this.state.verified.length !== 0
                        && this.state.verified !== undefined ?
                        this.state.verified.map((user) => (
                          <Dropdown.Item
                            onClick={() => {
                              console.log(user);
                            }}

                          >
                            {user.first_name + " " + user.last_name}
                          </Dropdown.Item>
                        ))
                        :
                        "  Još nema sudionika...  "
                    }
                  </Dropdown.Menu>
                </Dropdown>
                : ""
              }
              {this.props.id.id == this.state.id_organizer && this.state.id_type === 1 ?
                <Dropdown>
                  <Dropdown.Toggle
                    variant="dark"
                    id="dropdown-basic"
                    className="navigation-button-smaller"
                  >
                    <BsPersonPlus> </BsPersonPlus>
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    {
                      this.state.nonChecked.length !== 0
                        && this.state.verified !== undefined ?
                        this.state.nonChecked.map((user) => (
                          <Dropdown.Item
                            onClick={() => {
                              console.log(user);
                            }}

                          >
                            {user.firstName + " " + user.lastName}
                            <button type="button" className="btn-sm btn-success m-2" onClick={this.approve}> Odobri </button>
                            <button type="button" className="btn-sm btn-danger" onClick={this.remove}> Ukloni </button>
                          </Dropdown.Item>
                        ))
                        :
                        "  Nema aktivnih zahtjeva...  "
                    }
                  </Dropdown.Menu>
                </Dropdown>
                : ""
              }
            </div>
          </div>
          <div className="profile-container2" id="view2">
            <div className="profile-sidebar2">
              <div className="desktop-sticky-top">
                <br />
                {this.state.start_datetime !== "" ?
                  <h6
                    className="font-weight-bold"
                  >

                    {"START: " +
                      this.state.start_datetime.split("T")[0] +
                      " u " +
                      this.state.start_datetime.split("T")[1].split(":")[0] +
                      ":" +
                      this.state.start_datetime.split("T")[1].split(":")[1] +
                      "\n"}
                  </h6>
                  : ""}
                {this.state.start_datetime !== "" ?
                  <h6
                    className="font-weight-bold"
                  >
                    {
                      "END: " +
                      this.state.end_datetime.split("T")[0] +
                      " u " +
                      this.state.end_datetime.split("T")[1].split(":")[0] +
                      ":" +
                      this.state.end_datetime.split("T")[1].split(":")[1] +
                      "\n"
                    }
                  </h6>
                  : ""}
                <br />
                <h6 onClick={() => this.props.history.push(`/user/${this.state.id_organizer}`)}
                  className="font-weight-normal organizer-link"
                >Organizator: {this.state.firstName_organizer} {" "} {this.state.lastName_organizer}</h6>

                <h6
                  className="font-weight-normal m-3"
                >{this.state.description !== "" ? this.state.description : ""}</h6>

                <br />

              </div>
            </div>



            <div className="profile-content">

              <TableContainer>
                {" "}
                <div className="map-event" style={{ margin: "5%" }}>

                  <ReactMapGL
                    {...this.state.viewport}
                    {...this.state.settings}
                    dragPan={true}
                    mapboxApiAccessToken={TOKEN}
                    mapStyle="mapbox://styles/ivajnsajdbak/cklqsnvxe7hwf18nyevyle2jq"
                  >
                    {this.state !== undefined && this.state.long !== undefined
                      && this.state.lat !== undefined ?
                      <Marker
                        key={this.state.id_event}
                        longitude={15.966568}
                        latitude={45.815399}
                        offsetTop={-30}
                        offsetLeft={-15}
                      >
                        <img src={icon} style={{ width: "30px" }} />
                      </Marker>
                      : ""}
                  </ReactMapGL>

                </div>
              </TableContainer>

              <div className="attendence">
                <h6 className="btn btn-light my-btn">Dolazi: </h6>
                <Button size="lg" variant="secondary" class="btn btn-secondary" disabled>{this.state.attending_count}</Button>
                {this.state.id_type === 0 ?
                  <Button variant="success" size="lg" onClick={this.going} id="going" disabled={this.state.disabled || this.state.request_status != 0}
                  >
                    {this.state.request_status == 0 ? "Dolazim!" : "Dolazim! ✔️"}

                  </Button>
                  :
                  <Button active class="btn btn-primary btn-lg block" size="lg" id="going" style={{ width: "130px" }} disabled={this.state.disabled || this.state.request_status != 0} onClick={this.going} >
                    {this.state.request_status == 0 ? "Pošalji zahtjev!" : (this.state.request_status == 1 ? "Pending..." : "Zahtjev odobren ✔️")}
                  </Button>
                }
              </div>
            </div>
          </div>
        </div>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: () => dispatch(logoutUser()),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EventPage)
);
