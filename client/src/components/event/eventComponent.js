import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Button, List, Card } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import HorizontalScroll from "react-scroll-horizontal";
import moment from 'moment';
import "./eventComponent.css"

class EventComponent extends React.Component {
  state = {
    extended: undefined
  }

  getFormData = () => {
    return this.props.modelData;
  }

  formatItem = (item) => {
    return <div className="eventDiv"
    >
      <Card
        className="spotlight" >

        <div className="popup-title-profile">
          <p onClick={() => this.props.history.push(`/events/${item.id_event}`)}>
            {item.title}
          </p>
          <div className="event-info">
            <p className="start-event">
              Početak:{" "}
              <br />
              {item.start_datetime !== undefined ?
                item.start_datetime.split("T")[0] +
                " u " +
                item.start_datetime.split("T")[1].split(":")[0] +
                ":" +
                item.start_datetime.split("T")[1].split(":")[1] +
                "\n" : ""}
            </p>
            <p className="end-event">
              Kraj: {" "}
              <br />
              {item.end_datetime !== undefined
                ?
                item.end_datetime.split("T")[0] +
                " u " +
                item.end_datetime.split("T")[1].split(":")[0] +
                ":" +
                item.end_datetime.split("T")[1].split(":")[1]
                : ""}
            </p>
          </div>
        </div>
      </Card>

    </div>
  }

  render() {
    return (
      <div className="component-width" style={{ width: "100vw", height: "100vh" }}>
        <HorizontalScroll reverseScroll={true}>{this.props.modelData.map((item) => this.formatItem(item))}</HorizontalScroll>
        {/* <List
          className={["offers_list", this.state.extended && 'spotlight-background']}
          grid={{ gutter: 1, column: 2 }}
          size="large"
          dataSource={this.props.modelData}
          renderItem={item => (
            <List.Item>
              {this.formatItem(item)}
            </List.Item>
          )}
        /> */}
      </div>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (item) => dispatch(addToCart(item))
  }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventComponent));
