import React, { Component } from 'react'
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { updateUser } from '../../redux/actions'
import { authHeader } from "../../helpers/authHeader";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import {BACKEND_URL} from "../../helpers/constaints";

class EditEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            start: "",
            end: "",
            lat: "",
            long: "",
            id_organizer: "",
            organizer: "",
            attending_count: "",
            id_event: this.props.match.params.id,
            errors: {
                errors: [],
              },
        }
        this.handleChange = this.handleChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.cancel = this.cancel.bind(this);
    }
  
    handleChange() {
        var isChecked = document.getElementById("switch").checked;
        console.log(isChecked);
        if (isChecked) isChecked = 0;
        else isChecked = 1;
        this.setState({
          ...this.state,
          id_type: isChecked === "" || isChecked === false ? 0 : 1,
        });
        console.log(isChecked)
      }

  form = React.createRef();
  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      ...this.state,
      [name]: value,
    });
    console.log(value)
  };

  checkDate = () => {
    if (this.state.start >= this.state.end) {
      toast.error("Uneseni termin je neispravan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        zIndex: 1,
      });
      return true;
    }
    return false;
  }

  find(array, key) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] !== undefined) {
        return array[i];
      }
    }
    return null;
  }

  submit = (e) => {
    e.preventDefault();
    const fromData = this.state;
    const userId = JSON.parse(localStorage.getItem("user")).userId;
    const dataEvent = {
      id_event: fromData.id_event,
      title: fromData.title,
      description: fromData.description,
      start_datetime: fromData.start,
      end_datetime: fromData.end,
      id_type: fromData.id_type,
      id_organizer: userId,
    };

    console.log(dataEvent);

    var errors = [];

    if (this.state.title === "") {
      errors.push("title");
    }

    if (this.state.start === "") {
      errors.push("start");
    }

    if (this.state.end === "") {
      errors.push("end");
    }

    const { error } = this.state.errors;
    this.setState({
      error: {
        ...error,
        errors: errors,
      },
    });

    if (errors.length > 0) {
      console.log(errors)
      toast.error("Uneseni podatci nisu potpuni!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        zIndex: 1,
      });
      return false;
    } else {
      const options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: authHeader(),
        },
        body: JSON.stringify(dataEvent),
      };
      console.log(options.body);
      fetch(BACKEND_URL + `/events/edit`, options).then((response) => {
        if (response.status !== 200) {
          toast.error("❌  Neispravan unos!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        } else {
          response.json().then((response) => {
            console.log(response);
          });
          toast.success("Događaj dodan!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        }
        this.props.history.push(`/events/${this.props.match.params.id}`);
    });
    }
  }

  componentDidMount() {

    fetch(BACKEND_URL + `/events/${this.props.match.params.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
          Authorization: authHeader(),
        },
      }).then((response) => {
          response.json().then((json) => {
            console.log("json:" + JSON.stringify(json))
            console.log(this.props)
          this.setState({
              id: json.id_user,
              title: json.title,
              description: json.description,
              start: json.start_datetime,
              end: json.end_datetime,
              lat: json.lat,
              long: json.long,
              id_organizer: json.id_organizer,
          });
         console.log(this.state);
          })
        })
    }



  cancel = () => {
    this.props.history.push(`/events/${this.props.match.params.id}`);
  }

  onValuesChange = (changed, all) => {
    this.setState(changed, () => console.log(this.state))
  }

  render() {

    return (
        <div className="Login">
        {/* <div className="d-flex flex-column bg justify-content-between"> */}
        <div className="mx-auto mt-1 text-center">
          <label>
            <strong className="title-log">UREDI INFORMACIJE O DOGAĐAJU</strong>
          </label>
        </div>
        <form>
          <div className="next text-center">
            <div className="form-reg center justify-content-center mx-auto">
             
              <div className="mx-auto text-center divnext" id="fna">
                <input
                  className={
                    this.find(this.state.errors, "title") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="title"
                  placeholder="Naziv"
                  value={this.state.title}
                  onChange={this.onChange}
                ></input>
              </div>
              
              <div className="mx-auto text-center col-sm-12 my-1" id="use">
              <TextField
                  required
                  id="datetime-local"
                  name="start"
                  value={this.state.start}
                  label="Početak događaja"
                  type="datetime-local"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChange}
                  className={
                    this.find(this.state.errors, "start") !== null
                      ? "is-invalid"
                      : ""
                  }
                />
                <TextField
                  required
                  id="datetime-local"
                  name="end"
                  value={this.state.end}
                  label="Kraj događaja"
                  type="datetime-local"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChange}
                  onBlur={this.checkDate}
                  className={
                    this.find(this.state.errors, "end") !== null
                      ? "is-invalid"
                      : ""
                  }
                />
              </div>
             
              <div className="mx-auto text-center col-sm-12 my-1" id="ema">
                <TextField type="number" min="1" name="people" value={this.state.attending_count} id="guest-number" label="Broj ljudi" onChange={this.onChange}
                  className={
                    this.find(this.state.errors, "people") !== null
                      ? "is-invalid"
                      : ""
                  }
                />
              </div>

              <div className="mx-auto text-center col-sm-12 my-1" id="ema">
                <FormControlLabel
                    value="start"
                    control={<Switch id="switch" inputProps={{ role: 'switch' }} color="primary" onChange={this.handleChange} />}
                    label="Privatno:"
                    labelPlacement="start"
                />
              </div>
              <div className="mx-auto text-center" id="lna">
                <textarea
                  className={
                    this.find(this.state.errors, "description") !== null
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="description"
                  placeholder="Opis"
                  onChange={this.onChange}
                  value={this.state.description}
                ></textarea>
              </div>

              <button
                className="btn mt-2 text-align text-center btn-primary"
                style={{ border: "1px solid black" }}
                float="center"
                type="submit"
                onClick={this.submit}
              >
                SPREMI
              </button>
              <button
                className="btn mt-2 text-align text-center btn-secondary"
                style={{ border: "1px solid black" }}
                float="center"
                type="cancel"
                onClick={this.cancel}
              >
                ODUSTANI
              </button>
            </div>
          </div>
        </form>
        
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: (data) => dispatch(updateUser(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditEvent));
