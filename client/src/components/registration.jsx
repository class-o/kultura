import React, { useRef, useState, useEffect, useCallback } from "react";
import ReactMapGL, { Marker, Popup, GeolocateControl } from "react-map-gl";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./login.css";
import { createUser } from "../redux/actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { BACKEND_URL } from "../helpers/constaints";
import Pin from "./draggablePin";
import "./Registration.css";
const TOKEN =
  "pk.eyJ1IjoiaXZham5zYWpkYmFrIiwiYSI6ImNrbGFtNXpwNDA1djkyb28xdjE3czA3azcifQ.XdUshSAvDC3DoVtpKNJr2A";

const geolocateControlStyle = {
  right: 10,
  top: 10,
};

function Registration(props) {
  const mapRef = useRef();

  const [viewport, setViewport] = useState({
    latitude: 45.815399,
    longitude: 15.966568,
    zoom: 15,
    width: "98.5vw",
    height: "75vh",
    bearing: 0,
    pitch: 0,
  });

  const [settings, setsettings] = useState({
    dragPan: true,
    dragRotate: true,
    scrollZoom: true,
    touchZoom: true,
    touchRotate: true,
    keyboard: true,
    doubleClickZoom: true,
  });

  var [user, setUser] = useState([{}]);

  var [userSelected, setUserSelected] = React.useState(true);
  var [placeSelected, setPlaceSelected] = React.useState(false);
  var [errors, setErrors] = React.useState([]);
  const [events, logEvents] = useState({});

  const [marker, setMarker] = useState({
    latitude: 45.815399,
    longitude: 15.966568,
  });

  var onChange = (event) => {
    const { name, value } = event.target;
    setUser({
      ...user,
      [name]: value,
    });
  };

  useEffect(() => {
    var element = document.getElementsByClassName("user");
    if (element[0].style.color === "grey") {
      setUserSelected(false);
      setPlaceSelected(true);
    } else {
      setUserSelected(true);
      setPlaceSelected(false);
    }
  });

  var placeSelect = (event) => {
    if (userSelected) {
      setUserSelected(false);
      setPlaceSelected(true);
      event.target.style.color = "white";
      let element = document.getElementsByClassName("user");
      element[0].style.color = "grey";
      document.getElementById("fna").classList.toggle("d-none");
      document.getElementById("lna").classList.toggle("d-none");
      document.getElementById("use").classList.toggle("d-none");
      document.getElementById("fb").classList.toggle("d-none");
      document.getElementById("ggl").classList.toggle("d-none");
      document.getElementById("name").classList.toggle("d-none");
      document.getElementById("line").classList.toggle("d-none");
      document.getElementById("map").classList.toggle("d-none");
    }
  };

  var userSelect = (event) => {
    if (placeSelected) {
      setUserSelected(true);
      setPlaceSelected(false);
      event.target.style.color = "white";
      let element = document.getElementsByClassName("place");
      element[0].style.color = "grey";
      document.getElementById("fna").classList.toggle("d-none");
      document.getElementById("lna").classList.toggle("d-none");
      document.getElementById("fb").classList.toggle("d-none");
      document.getElementById("ggl").classList.toggle("d-none");
      document.getElementById("use").classList.toggle("d-none");
      document.getElementById("name").classList.toggle("d-none");
      document.getElementById("line").classList.toggle("d-none");
      document.getElementById("map").classList.toggle("d-none");
    }
  };

  var checkPassword = () => {
    return user.password !== user.confirmPassword;
  };

  var checkPasswordLength = () => {
    if (
      user.password !== undefined &&
      (user.password.length < 6 || user.password.length > 20)
    ) {
      toast.error("❌ Lozinka mora sadržavati između 6 i 20 znakova!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return false;
    }
    return true;
  };

  var usernameCheck = (event) => {
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    fetch(
      BACKEND_URL + `/user/username-available/?username=${user.username}`,
      options
    ).then((response) => {
      if (response.status === 400) {
        toast.error("❌ Korisničko ime nije slobodno!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
  };

  var emailCheck = (event) => {
    const options = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    fetch(BACKEND_URL + `/user/email-available/?email=${user.email}`, options)
      .then((response) => {
        if (response.status === 400) {
          toast.error("❌ Email se već koristi!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return false;
        }
      })
      .catch();
    const expression = /\S+@\S+/;
    var checkAt = expression.test(String(user.email).toLowerCase());
    if (user.email !== "" && user.email.len != 0 && !checkAt) {
      toast.error("❌ Email treba sadržavati znak @!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return false;
    }
    return true;
  };

  const find = (array, key) => {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] !== undefined) {
        return array[i];
      }
    }
    return null;
  };

  const onMarkerDragStart = useCallback((event) => {
    logEvents((_events) => ({ ..._events, onDragStart: event.lngLat }));
  }, []);

  const onMarkerDrag = useCallback((event) => {
    logEvents((_events) => ({ ..._events, onDrag: event.lngLat }));
  }, []);

  const onMarkerDragEnd = useCallback((event) => {
    logEvents((_events) => ({ ..._events, onDragEnd: event.lngLat }));
    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
    });
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();

    const fromData = user;

    const dataUser = {
      id: fromData.id,
      firstName: fromData.firstName,
      lastName: fromData.lastName,
      username: fromData.username,
      password: fromData.password,
      email: fromData.email,
    };

    const dataPlace = {
      name: fromData.name,
      password: fromData.password,
      email: fromData.email,
      lat: marker.latitude,
      long: marker.longitude,
      verified: false,
      checked: false,
    };

    console.log(dataUser);
    console.log(dataPlace);

    var errors = [];

    if (user.username === "" && userSelected) {
      errors.push("username");
    }

    if (user.name === "" && placeSelected) {
      errors.push("name");
    }

    if (user.firstName === "" && userSelected) {
      errors.push("firstName");
    }

    if (user.lastName === "" && userSelected) {
      errors.push("lastName");
    }

    if (!emailCheck()) {
      errors.push("email");
    }

    if (!checkPasswordLength()) {
      errors.push("password");
    }

    setErrors({
      ...errors,
      errors: errors,
    });

    if (errors.length > 0) {
      return false;
    }

    if (userSelected) {
      const options1 = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataUser),
      };

      fetch(BACKEND_URL + `/user/register`, options1).then((response) => {
        if (response.status === 400) {
          toast.error("❌ Registracija nije moguća!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        response.json().then((json) => {
          console.log(json);
          this.props.dispatch({
            id: json.id,
            userType: "user",
          });
          localStorage.setItem(
            "user",
            JSON.stringify({
              id: json.id,
              userType: "user",
              ...{
                authdata: btoa(
                  `${requestBodyUser.username}:${requestBodyUser.password}`
                ),
              },
            })
          );
          this.props.history.push("/");
        });
      });
    } else {
      const options1 = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataPlace),
      };

      fetch(BACKEND_URL + `/place/register`, options1).then((response) => {
        if (response.status === 400) {
          toast.error("❌ Registracija nije moguća!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.success("Uspješno dodano mjesto!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            zIndex: 1,
          });
        }
      });
    }
  };

  return (
    <div className="Login">
      <div className="mx-auto mt-1 text-center">
        <label>
          <strong className="title-log">KULTURA</strong>
        </label>
      </div>
      <div className="mx-auto mt-2 text-center">
        <label>
          <span
            className="user"
            style={{ color: "white", fontSize: "18px" }}
            onClick={userSelect}
          >
            KORISNIK
          </span>
          <span style={{ color: "white", fontSize: "20px" }}>
            {"  "} | {"  "}
          </span>
          <span
            className="place"
            style={{ color: "gray", fontSize: "18px" }}
            onClick={placeSelect}
          >
            MJESTO
          </span>
        </label>
      </div>
      <form>
        <div className="next text-center">
          <div className="form-reg center justify-content-center mx-auto">
            <a
              id="fb"
              className="btn btn-primary col-sm-12 text-align text-center my-1"
              href="http://localhost:5000/auth/facebook"
              role="button"
            >
              FACEBOOK
            </a>
            <a
              id="ggl"
              className="btn btn-danger col-sm-12 text-align text-center my-1"
              href="http://localhost:5000/auth/google"
              role="button"
            >
              GOOGLE
            </a>
            <hr id="line" />
            <span className="space title-log">Sign up using email</span>
            <div className="mx-auto text-center divnext" id="fna">
              <input
                className={
                  find(errors, "firstName") !== null
                    ? "form-control is-invalid"
                    : "form-control"
                }
                name="firstName"
                placeholder="First name"
                onChange={onChange}
              ></input>
            </div>

            <div className="mx-auto text-center divnext" id="lna">
              <input
                className={
                  find(errors, "lastName") !== null
                    ? "form-control is-invalid"
                    : "form-control"
                }
                name="lastName"
                placeholder="Last name"
                onChange={onChange}
                value={user.lastName}
              ></input>
            </div>

            <div className="mx-auto text-center col-sm-12 my-1" id="use">
              <input
                className={
                  find(errors, "username") !== null
                    ? "form-control is-invalid"
                    : "form-control"
                }
                name="username"
                placeholder="Username"
                onChange={onChange}
                onBlur={usernameCheck}
                value={user.username}
                required
                isValid="false"
              ></input>
            </div>

            <div
              className="mx-auto text-center col-sm-12 my-1 d-none"
              id="name"
            >
              <input
                className={
                  find(errors, "name") !== null
                    ? "form-control is-invalid"
                    : "form-control"
                }
                name="name"
                placeholder="Name"
                onChange={onChange}
                value={user.name}
                required
                isValid="false"
              ></input>
            </div>

            <div className="mx-auto text-center col-sm-12 my-1" id="ema">
              <input
                className={
                  find(errors, "email") !== null
                    ? "form-control is-invalid"
                    : "form-control"
                }
                name="email"
                type="email"
                placeholder="Email"
                onChange={onChange}
                onBlur={emailCheck}
                value={user.email}
                required
              ></input>
            </div>

            <div className="mx-auto text-center col-sm-12 my-1" id="pas">
              <input
                className={
                  find(errors, "password") !== null
                    ? "form-control is-invalid"
                    : "form-control"
                }
                name="password"
                type="password"
                placeholder="Password"
                onChange={onChange}
                value={user.password}
                onBlur={checkPasswordLength}
                required
              ></input>
            </div>
            <div className="mx-auto text-center col-sm-12 my-1" id="cpa">
              <input
                className={
                  checkPassword() ? "form-control is-invalid" : "form-control"
                }
                name="confirmPassword"
                placeholder="Repeat password"
                type="password"
                onChange={onChange}
                value={user.confirmPassword}
                required
              ></input>
            </div>

            <div
              className="mapa mx-auto text-center col-sm-12 my-1 d-none"
              id="map"
            >
              <ReactMapGL
                {...viewport}
                {...settings}
                ref={mapRef}
                dragPan={true}
                mapboxApiAccessToken={TOKEN}
                mapStyle="mapbox://styles/ivajnsajdbak/cklqsnvxe7hwf18nyevyle2jq"
                onViewportChange={(v) => setViewport(v)}
                style={{ width: "inherit", height: "300px" }}
              >
                {/* <Geocoder
                  mapRef={mapRef}
                  onViewportChange={handleGeocoderViewportChange}
                  mapboxApiAccessToken={TOKEN}
                  position="top-left"
                  bbox={[13, 42, 20, 47]} // Boundary for Berkeley
                  proximity={{
                    longitude: 15.9665,
                    latitude: 45.815339,
                  }}
                /> */}
                <GeolocateControl
                  style={geolocateControlStyle}
                  positionOptions={{ enableHighAccuracy: true }}
                  trackUserLocation={true}
                  auto
                />
                <Marker
                  longitude={marker.longitude}
                  latitude={marker.latitude}
                  offsetTop={-20}
                  offsetLeft={-10}
                  draggable
                  onDragStart={onMarkerDragStart}
                  onDrag={onMarkerDrag}
                  onDragEnd={onMarkerDragEnd}
                >
                  <Pin size={20} />
                </Marker>
              </ReactMapGL>
            </div>

            <button
              className="btn mt-2 text-align text-center btn-primary"
              style={{ border: "1px solid black" }}
              float="center"
              type="submit"
              onClick={onSubmit}
              disabled={checkPassword()}
            >
              REGISTRACIJA
            </button>
          </div>
        </div>
      </form>
      <div className="mx-auto text-center col-auto col-sm-8 my-3 justify-content-center">
        <span className="space title-log">Već ste korisnik?</span>
        <a
          className="btn btn-dark mt-2 text-align text-center"
          href="/login"
          role="button"
        >
          LOGIN
        </a>
      </div>
      <ToastContainer />
      <script src="./baloons/baloons.js"></script>
      <script src="./baloons/baloons.css"></script>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (user) => dispatch(createUser(user)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Registration)
);
