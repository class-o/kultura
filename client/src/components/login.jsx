import React from "react";
import "./login.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";
import { BACKEND_URL } from "../helpers/constaints";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      email: "",
      title: {
        userSelected: true,
        placeSelected: false,
      },
    };

    this.onChange = this.onChange.bind(this);
  }

  placeSelect = (event) => {
    if (this.state.title.userSelected) {
      this.state.title.userSelected = false;
      this.state.title.placeSelected = true;
      event.target.style.color = "white";
      let element = document.getElementsByClassName("user");
      element[0].style.color = "grey";
      document.getElementById("mail").classList.toggle("d-none");
      document.getElementById("username").classList.toggle("d-none");
      document.getElementById("fb").classList.toggle("d-none");
      document.getElementById("ggl").classList.toggle("d-none");
      document.getElementById("line").classList.toggle("d-none");
    }
  };

  userSelect = (event) => {
    if (this.state.title.placeSelected) {
      this.state.title.userSelected = true;
      this.state.title.placeSelected = false;
      event.target.style.color = "white";
      let element = document.getElementsByClassName("place");
      element[0].style.color = "grey";
      document.getElementById("mail").classList.toggle("d-none");
      document.getElementById("username").classList.toggle("d-none");
      document.getElementById("fb").classList.toggle("d-none");
      document.getElementById("ggl").classList.toggle("d-none");
      document.getElementById("line").classList.toggle("d-none");
    }
  };

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const loginData = this.state;

    const requestBodyUser = {
      username: loginData.username,
      password: loginData.password,
    };

    const requestBodyPlace = {
      username: loginData.email,
      password: loginData.password,
    };

    if (loginData.title.userSelected) {
      const options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify(requestBodyUser),
      };

      fetch(BACKEND_URL + `/auth/local`, options).then((response) => {
        if (response.status === 400) {
          toast.error("❌ Korisnik ne postoji!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        response.json().then((json) => {
          this.props.dispatch({
            id: json.id,
            userType: json.type == 1 ? "admin" : "user",
          });
          localStorage.setItem(
            "user",
            JSON.stringify({
              id: json.id,
              userType: json.type == 1 ? "admin" : "user",
              ...{
                authdata: btoa(
                  `${requestBodyUser.username}:${requestBodyUser.password}`
                ),
              },
            })
          );
          this.props.history.push("/");
        });
      });
    } else {
      const options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify(requestBodyPlace),
      };

      fetch(BACKEND_URL + `/place/login`, options).then((response) => {
        if (response.status === 400) {
          toast.error("❌ Korisnik ne postoji!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        response.json().then((json) => {
          console.log(json);
          this.props.dispatch({
            id: json.id,
            userType: "mjesto",
          });
          localStorage.setItem(
            "user",
            JSON.stringify({
              id: json.id,
              userType: "mjesto",
              ...{
                authdata: btoa(
                  `${requestBodyUser.username}:${requestBodyUser.password}`
                ),
              },
            })
          );
          this.props.history.push("/");
        });
      });
    }
  };
  render() {
    return (
      <div className="Login">
        <div className="mx-auto mt-2 text-center">
          <label>
            <strong className="title-log">KULTURA</strong>
          </label>
        </div>
        <div className="mx-auto mt-2 text-center">
          <label>
            <span
              className="user"
              style={{ color: "white", fontSize: "18px" }}
              onClick={this.userSelect}
            >
              KORISNIK
            </span>
            <span style={{ color: "white", fontSize: "20px" }}>
              {"  "} | {"  "}
            </span>
            <span
              className="place"
              style={{ color: "gray", fontSize: "18px" }}
              onClick={this.placeSelect}
            >
              MJESTO
            </span>
          </label>
        </div>
        <form>
          <div className="next text-center">
            <div className="form-reg center justify-content-center mx-auto">
              <a
                id="fb"
                className="btn btn-primary col-sm-12 text-align text-center my-1"
                href="http://localhost:5000/auth/facebook"
                role="button"
              >
                FACEBOOK
              </a>
              <a
                id="ggl"
                className="btn btn-danger col-sm-12 text-align text-center my-1"
                href="http://localhost:5000/auth/google"
                role="button"
              >
                GOOGLE
              </a>
              <hr id="line" />
              <span className="space title-log" id="title">
                Sign In Using Email
              </span>
              <div className="mx-auto text-center col-sm-12 my-3" id="username">
                <input
                  className="form-control"
                  name={"username"}
                  type="username"
                  placeholder="Username"
                  onChange={this.onChange}
                  value={this.state.username}
                ></input>
              </div>
              <div
                className="mx-auto text-center col-sm-12 my-3 d-none"
                id="mail"
              >
                <input
                  className="form-control"
                  name={"email"}
                  type="email"
                  placeholder="Email"
                  onChange={this.onChange}
                  value={this.state.email}
                ></input>
              </div>
              <div className="mx-auto text-center col-sm-12 my-3">
                <input
                  className="form-control"
                  name={"password"}
                  placeholder="Password"
                  type="password"
                  onChange={this.onChange}
                  value={this.state.password}
                ></input>
              </div>
              <button
                className="btn btn-primary mt-2 text-align text-center"
                float="center"
                type="submit"
                onClick={this.onSubmit}
              >
                PRIJAVA
              </button>
            </div>
          </div>
        </form>
        <div className="mx-auto text-center col-auto col-sm-8 my-3 justify-content-center">
          <span className="space title-log">Niste korisnik?</span>
          <a
            className="btn btn-dark mt-2 text-align text-center"
            href="/registration"
            role="button"
          >
            REGISTRACIJA
          </a>
        </div>
        <ToastContainer />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (user) => dispatch(loginUser(user)),
  };
};

const mapStateToProps = (state) => {
  return {
    id: state.user.id,
    logged: state.user.logged,
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
