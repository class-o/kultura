import _ from "lodash";
import faker from "faker";
import React from "react";
import { Search, Grid } from "semantic-ui-react";
import { BACKEND_URL } from "../helpers/constaints";
import { useHistory } from "react-router-dom";
import { ConsoleSqlOutlined } from "@ant-design/icons";

const initialState = {
  loading: false,
  results: [],
  value: "",
};

function exampleReducer(state, action) {
  switch (action.type) {
    case "CLEAN_QUERY":
      return initialState;
    case "START_SEARCH":
      return { ...state, loading: true, value: action.query };
    case "FINISH_SEARCH":
      return { ...state, loading: false, results: action.results };
    case "UPDATE_SELECTION":
      return { ...state, value: action.selection };

    default:
      throw new Error();
  }
}

function SearchExampleStandard() {
  const [state, dispatch] = React.useReducer(exampleReducer, initialState);
  const { loading, results, value } = state;
  //pri prvom ucitavanju i reloadu nije htjelo raditi pa smo koristili let umjesto stateove
  // const [users, setUsers] = React.useState([]);
  // const [places, setPlaces] = React.useState([]);
  // const [events, setEvents] = React.useState([]);
  let users = [];
  let places = [];
  let events = [];

  const history = useHistory();

  const timeoutRef = React.useRef();
  const handleSearchChange = React.useCallback((e, data) => {
    clearTimeout(timeoutRef.current);
    dispatch({ type: "START_SEARCH", query: data.value });

    let all = [];
    users.map((user) => {
      let fakeuser = {
        title: user.username,
        description: "korisnik",
        id: user.id_user,
        url: "/user/" + user.id_user,
      };
      all = [...all, fakeuser];
    });
    places.map((place) => {
      let fakeplace = {
        title: place.name,
        description: "mjesto",
        id: place.id_place,
        url: "/place/" + place.id_place,
      };
      all = [...all, fakeplace];
    });
    events.map((event) => {
      let fakeevent = {
        title: event.title,
        description: "događaj",
        id: event.id_event,
        url: "/events/" + event.id_event,
      };
      all = [...all, fakeevent];
    });
    console.log(all);

    timeoutRef.current = setTimeout(() => {
      if (data.value.length === 0) {
        dispatch({ type: "CLEAN_QUERY" });
        return;
      }

      const re = new RegExp(_.escapeRegExp(data.value), "i");
      const isMatch = (result) => re.test(result.title);

      dispatch({
        type: "FINISH_SEARCH",
        results: _.filter(all, isMatch),
      });
    }, 300);
  }, []);

  function fetchAllData() {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
    };
    fetch(`${BACKEND_URL}/user/all`, requestOptions).then((response) => {
      response.json().then((json) => {
        users = json;
      });
    });
    fetch(`${BACKEND_URL}/events/all`, requestOptions).then((response) => {
      response.json().then((json) => {
        events = json;
        console.log("EVENT");
      });
    });
    fetch(`${BACKEND_URL}/place/all/`, requestOptions).then((response) => {
      response.json().then((json) => {
        places = json;
      });
    });
  }

  React.useEffect(() => {
    return () => {
      clearTimeout(timeoutRef.current);
    };
  }, []);

  return (
    <Grid>
      <Grid.Column width={6}>
        <Search
          onFocus={() => fetchAllData()}
          loading={loading}
          onResultSelect={(e, data) => history.push(data.result.url)}
          onSearchChange={handleSearchChange}
          results={results}
          value={value}
        />
      </Grid.Column>
    </Grid>
  );
}

export default SearchExampleStandard;
