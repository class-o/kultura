import React, { useRef, useState, useEffect, useCallback } from "react";
import ReactMapGL, { Marker, Popup, GeolocateControl } from "react-map-gl";
import "./home.css";
import AddEvent from "./AddEvent";
import { ToastContainer, toast } from "react-toastify";
import { Fab, Action } from "react-tiny-fab";
import "react-tiny-fab/dist/styles.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../redux/actions";
import {
  BsGeo,
  BsSearch,
  BsArrowCounterclockwise,
  BsGeoAlt,
  BsPlus,
  BsBullseye,
} from "react-icons/bs";
import { BsCalendar } from "react-icons/bs";
import { BACKEND_URL } from "../helpers/constaints";
import EventComponent from "./event/eventComponent";
import { Scrollbars } from "react-custom-scrollbars";
import { RiArrowDownSLine } from "react-icons/ri";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import { DateRangePicker } from "react-dates";
import moment from "moment";
import { Button } from "react-bootstrap";
import Pin from "./draggablePin";
import "react-map-gl-geocoder/dist/mapbox-gl-geocoder.css";
import "./react-dates.css";
const TOKEN =
  "pk.eyJ1IjoiaXZham5zYWpkYmFrIiwiYSI6ImNrbGFtNXpwNDA1djkyb28xdjE3czA3azcifQ.XdUshSAvDC3DoVtpKNJr2A";

const geolocateControlStyle = {
  right: 10,
  top: 10,
};

const scrollToRef = (ref) =>
  window.scrollTo({ top: ref.current.offsetTop, behavior: "smooth" });

function Map(props) {
  const [viewport, setViewport] = useState({
    latitude: 45.815399,
    longitude: 15.966568,
    zoom: 12,
    width: "98.5vw",
    height: "75vh",
    bearing: 0,
    pitch: 0,
  });

  const [settings, setsettings] = useState({
    dragPan: true,
    dragRotate: true,
    scrollZoom: true,
    touchZoom: true,
    touchRotate: true,
    keyboard: true,
    doubleClickZoom: true,
  });

  const [selected, setSelected] = useState(null);

  const [visible, setVisible] = useState();

  const [zagrebAll, setZagrebAll] = useState([{}]);

  const [hangsAll, setHangsAll] = useState([{}]);

  const [startDate, setStartDate] = React.useState(moment());
  const [endDate, setEndDate] = React.useState(moment().add(7, "d"));
  const [focusedInput, setFocusedInput] = React.useState(null);

  const [currentPosition, setCurrentPosition] = React.useState({
    latitude: 45.815399,
    longitude: 15.966568,
  });

  const [marker, setMarker] = useState({
    latitude: currentPosition.latitude,
    longitude: currentPosition.longitude,
  });

  const [events, logEvents] = useState({});

  const onMarkerDragStart = useCallback((event) => {
    logEvents((_events) => ({ ..._events, onDragStart: event.lngLat }));
  }, []);

  const onMarkerDrag = useCallback((event) => {
    logEvents((_events) => ({ ..._events, onDrag: event.lngLat }));
  }, []);

  const onMarkerDragEnd = useCallback((event) => {
    logEvents((_events) => ({ ..._events, onDragEnd: event.lngLat }));
    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
    });
  }, []);

  var openEventPage = (event) => {
    if (selected != null) {
      props.history.push(`events/${event.id_event}`);
    }
  };

  const myRef = useRef(null);
  const mapRef = useRef();
  const executeScroll = () => scrollToRef(myRef);

  function updateMap() {
    fetch(BACKEND_URL + `/events/all`).then((response) => {
      response.json().then((json) => {
        json.map((tag) => {
          tag.long = parseFloat(tag.long.match(/^-?\d+(?:\.\d{0,8})?/)[0]);
          tag.lat = parseFloat(tag.lat.match(/^-?\d+(?:\.\d{0,8})?/)[0]);
        });
        setZagrebAll(json);
      });
    });
    setMarker({
      latitude: currentPosition.latitude,
      longitude: currentPosition.longitude,
    });
    fetch(BACKEND_URL + `/hangouts/all`).then((response) => {
      response.json().then((json) => {
        json.map((tag) => {
          tag.long = parseFloat(tag.long.match(/^-?\d+(?:\.\d{0,8})?/)[0]);
          tag.lat = parseFloat(tag.lat.match(/^-?\d+(?:\.\d{0,8})?/)[0]);
        });
        setHangsAll(json);
      });
    });
  }

  function addPersonToHang(event_id) {
    console.log(event_id);
    const hangBody2 = {
      id_user: props.id,
      id_hangout: event_id,
    };
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify(hangBody2),
    };
    fetch(BACKEND_URL + `/hangouts/add-user`, options).then((response) => {
      console.log("Uspijo addat usera to hang");
      if (response.status === 400) {
        toast.error("Već si se registrirao na ovo okupljanje! 😦", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          zIndex: 1,
        });
      } else {
        toast.success("Ideš!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          zIndex: 1,
        });
      }
    });
  }

  function createHang() {
    const hangBody = {
      id_user: props.id,
      long: currentPosition.longitude,
      lat: currentPosition.latitude,
    };
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify(hangBody),
    };
    fetch(BACKEND_URL + `/hangouts/create`, options).then((response) => {
      if (response.status === 400) {
        toast.error("Neš ne valja!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          zIndex: 1,
        });
      } else {
        toast.success("Dodano okupljanje!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          zIndex: 1,
        });
      }
    });
    updateMap();
  }

  useEffect(() => {
    function handleResize() {
      setViewport(viewport);
    }

    updateMap();

    const listener = (e) => {
      if (e.key === "Escape") {
        setSelected(null);
      }
    };

    navigator.geolocation.getCurrentPosition((pos) => {
      setCurrentPosition({
        latitude: pos.coords.latitude,
        longitude: pos.coords.longitude,
      });
    });
    window.addEventListener("keydown", listener);

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, []);

  function datesChanged() {
    fetch(
      BACKEND_URL +
        `/events/all?start_date=${startDate.format()}&&end_date=${endDate.format()}`
    ).then((response) => {
      response.json().then((json) => {
        json.map((tag) => {
          tag.long = parseFloat(tag.long.match(/^-?\d+(?:\.\d{0,8})?/)[0]);
          tag.lat = parseFloat(tag.lat.match(/^-?\d+(?:\.\d{0,8})?/)[0]);
        });
        setZagrebAll(json);
      });
    });
  }

  function changeVisibility(visibility) {
    setVisible(visibility);
  }

  return (
    <div className="main-home-container">
      <div className="map">
        <ReactMapGL
          {...viewport}
          {...settings}
          ref={mapRef}
          dragPan={true}
          mapboxApiAccessToken={TOKEN}
          mapStyle="mapbox://styles/ivajnsajdbak/cklqsnvxe7hwf18nyevyle2jq"
          onViewportChange={(v) => setViewport(v)}
        >
          {/* <Geocoder
            mapRef={mapRef}
            onViewportChange={handleGeocoderViewportChange}
            mapboxApiAccessToken={TOKEN}
            position="top-left"
            bbox={[13, 42, 20, 47]} // Boundary for Berkeley
            proximity={{
              longitude: 15.9665,
              latitude: 45.815339,
            }}
          /> */}
          <GeolocateControl
            style={geolocateControlStyle}
            positionOptions={{ enableHighAccuracy: true }}
            trackUserLocation={true}
            auto
          />
          <Marker
            longitude={marker.longitude}
            latitude={marker.latitude}
            offsetTop={-20}
            offsetLeft={-10}
            draggable
            onDragStart={onMarkerDragStart}
            onDrag={onMarkerDrag}
            onDragEnd={onMarkerDragEnd}
          >
            <Pin size={20} />
          </Marker>
          {zagrebAll !== undefined &&
          zagrebAll.length !== 0 &&
          zagrebAll[0].lat !== undefined &&
          zagrebAll[0].long !== undefined
            ? zagrebAll.map((tag) => (
                <Marker
                  key={tag.id_event}
                  longitude={tag.long}
                  latitude={tag.lat}
                  offsetTop={-30}
                  offsetLeft={-15}
                >
                  <button
                    style={{
                      backgroundColor: "transparent",
                      width: "20px",
                      border: "none",
                    }}
                    onClick={(e) => {
                      e.preventDefault();
                      setSelected(tag);
                    }}
                  >
                    <BsGeoAlt size={40} viewBox="0 0 24 24"></BsGeoAlt>
                  </button>
                </Marker>
              ))
            : ""}
          {hangsAll !== undefined &&
          hangsAll.length !== 0 &&
          hangsAll[0].lat !== undefined &&
          hangsAll[0].long !== undefined
            ? hangsAll.map((tag) => (
                <Marker
                  key={tag.id_hangout}
                  longitude={tag.long}
                  latitude={tag.lat}
                  offsetTop={-30}
                  offsetLeft={-15}
                >
                  <div className="pulse">
                    <button
                      className="bullseye"
                      style={{
                        backgroundColor: "transparent",
                        width: "20px",
                        border: "none",
                      }}
                      onClick={(e) => {
                        e.preventDefault();
                        addPersonToHang(tag.id_hangout);
                      }}
                    >
                      <BsBullseye size={40} viewBox="0 0 24 24"></BsBullseye>
                    </button>
                  </div>
                </Marker>
              ))
            : ""}
          {selected ? (
            <Popup
              latitude={selected.lat}
              longitude={selected.long}
              onClose={() => {
                openEventPage(selected);
                setSelected(null);
              }}
            >
              <div className="popup-custom">
                <p className="popup-title" style={{ marginBottom: "0" }}>
                  {selected.title}
                </p>
                <div className="title-organiser">
                  <p className="popup-date">
                    START:{" "}
                    {selected.start_datetime.split("T")[0] +
                      " u " +
                      selected.start_datetime.split("T")[1].split(":")[0] +
                      ":" +
                      selected.start_datetime.split("T")[1].split(":")[1] +
                      "\n"}
                  </p>
                  <p className="popup-date end">
                    END:{" "}
                    {selected.end_datetime.split("T")[0] +
                      " u " +
                      selected.end_datetime.split("T")[1].split(":")[0] +
                      ":" +
                      selected.end_datetime.split("T")[1].split(":")[1]}
                  </p>
                </div>
              </div>
            </Popup>
          ) : null}
        </ReactMapGL>
        <Fab
          mainButtonStyles={{
            zIndex: "1 !important",
            height: "45px",
            width: "45px",
          }}
          alwaysShowTitle={true}
          icon={<BsPlus style={{ color: "white" }} />}
        >
          <Action
            text="Organizirani događaj"
            onClick={(e) => {
              e.preventDefault();
              if (!props.logged) {
                toast.error(
                  "Trebate biti ulogirani kako bi koristili ovu funkcionalnost!",
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    zIndex: 1,
                  }
                );
              } else {
                toast.info(
                  "Crvena strelica treba pokazivati na lokaciju događaja!",
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    zIndex: 1,
                  }
                );
                setVisible(true);
                if (!("geolocation" in navigator)) {
                  toast.error(
                    "Funkcionalnost aplikacije je ograničena bez vaše loakcije!",
                    {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: true,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true,
                      progress: undefined,
                      zIndex: 1,
                    }
                  );
                }
              }
            }}
          >
            <BsCalendar style={{ color: "white" }}></BsCalendar>
          </Action>
          <Action
            text="Okupljanje"
            onClick={() => {
              createHang();
            }}
          >
            <i className="fa fa-help" />
            <BsGeo style={{ color: "white" }}></BsGeo>
          </Action>
        </Fab>
      </div>
      <div className="addEvent">
        {visible && (
          <AddEvent
            id={props.id}
            visible={visible}
            onVisibleChange={changeVisibility}
            userLocation={marker}
            addNewEvent={updateMap}
            userId
          />
        )}
      </div>
      <div className="datePicker">
        <DateRangePicker
          startDate={startDate}
          startDateId="startDate"
          endDate={endDate}
          endDateId="endDate"
          onDatesChange={({ startDate, endDate }) => {
            setStartDate(startDate);
            setEndDate(endDate);
          }}
          focusedInput={focusedInput}
          onFocusChange={setFocusedInput}
          // isOutsideRange={(day) => !isInclusivelyBeforeDay(day, moment())}
          initialVisibleMonth={() => moment()}
          numberOfMonths={1}
          orientation={"horizontal"}
          openDirection="up"
          numberOfMonths={1}
          // showDefaultInputIcon
          // inputIconPosition="after"
          startDatePlaceholderText="Početak"
          endDatePlaceholderText="Kraj"
          monthFormat="DD. MM. YYYY."
        />
        <Button
          variant="secondary"
          className="searchButton"
          onClick={datesChanged}
        >
          <BsArrowCounterclockwise
            className="searchIcon"
            style={{ color: "white" }}
          />
        </Button>
      </div>
      <div className="scroll" ref={myRef}>
        <RiArrowDownSLine
          onClick={executeScroll}
          className="arrow"
          color="white"
          size="50px"
        ></RiArrowDownSLine>
      </div>
      <div className="events">
        {zagrebAll !== undefined ? (
          <EventComponent
            className="event-home"
            modelData={zagrebAll}
          ></EventComponent>
        ) : (
          ""
        )}{" "}
      </div>
      <ToastContainer></ToastContainer>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    logged: state.user.logged,
    id: state.user.id,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: () => dispatch(logoutUser()),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Map));
