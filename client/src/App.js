import React, { Component } from 'react';
import Map from './components/Home';
import './App.css';
import Login from './components/login';
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './redux/store';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Navigation from './components/navbar';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Registration from './components/registration';
import Redirect from './components/Redirect';
import UserProfile from './components/user/UserProfile';
import EditUserProfile from './components/user/EditUserProfile';
import EventPage from './components/event/EventPage';
import EditEvent from './components/event/EditEvent';
import AdminProfile from './components/user/admin';
import Default from './components/Default'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router>
            <Navigation />
            <Switch>
              <Route exact path="/" component={Map} />
              <Route path='/login' component={Login} />
              <Route path='/registration' component={Registration} />
              <Route path='/userprofile' component={UserProfile} />
              <Route path='/editEvent/:id' component={EditEvent} />
              <Route path='/user/:id' component={UserProfile} />
              <Route path='/redirect/:id' component={Redirect} />
              <Route path='/events/:id' component={EventPage} />
              <Route path="/editProfile" component={EditUserProfile} />
              <Route path="/adminProfile" component={AdminProfile} />
              <Route path="*" component={Default} />
                
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
