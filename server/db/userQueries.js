const db = require(".");

module.exports = {

    emailAvailable: async email => {
        const emailSql = `SELECT id_user FROM users WHERE 
        email = '${email}';`;

        try {
            const result = await db.query(emailSql, []);
            return result.length == 0 ? true : false;
        } catch (err) {
            console.log(err);
            throw err
        }

    },

    usernameAvailable: async username => {
        const usernameSql = `SELECT id_user FROM users WHERE 
        username = '${username}';`;
        try {
            const result = await db.query(usernameSql, []);
            return result.length == 0 ? true : false;
        } catch (err) {
            console.log(err);
            throw err
        }

    },

    getUserInfo: async id => {
        const idSql = `SELECT * FROM users WHERE 
        id_user = '${id}';`;

        try {
            const result = await db.query(idSql, []);
            return result[0];
        } catch (err) {
            console.log(err);
        }
    },

    checkUsers: async email => {
        const emailSql = `SELECT id_user FROM users WHERE 
        email = '${email}';`;

        try {
            const result = await db.query(emailSql, []);
            return result;
        } catch (err) {
            console.log(err);
        }
    },

    getLoginInfo: async (username) => {
        const user = `SELECT id_user, username, password, type FROM users WHERE username = '${username}';`
        try {
            const result = await db.query(user);
            return result;
        } catch (err) {
            console.log(err);
        }
    },

    getAllUsers: async () => {
        const sql = `SELECT id_user,username, first_name, last_name, picture FROM users;`

        try {
            const all = await db.query(sql);
            if (all.length == 0) {
                return false;
            }
            console.log(all);
            return all;
        } catch (err) {
            throw err;
        }
    }

}