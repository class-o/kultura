const db = require(".");

module.exports = {

    createUser: async (username, first_name, last_name, email, gender, birthday, picture) => {

        let insertUser = `INSERT INTO users (username, first_name, last_name, email, gender, birthday, picture) VALUES 
        ('${username}', '${first_name}', '${last_name}', '${email}', '${gender}', '${birthday}', '${picture}');`


        const select = `SELECT id_user FROM users WHERE username = '${username}';`;

        try {
            await db.query(insertUser, []);
            const result = await db.query(select, []);
            return result[0];
        } catch (err) {
            console.log(err);
        }
    },

    createLocalUser: async (username, password, first_name, last_name, email) => {

        let insert = `INSERT INTO users (username, password, first_name, last_name, email) VALUES 
        ('${username}', '${password}', '${first_name}', '${last_name}', '${email}');`


        const select = `SELECT id_user FROM users WHERE username = '${username}';`;

        try {
            await db.query(insert, []);
            const result = await db.query(select, []);
            return result[0];
        } catch (err) {
            console.log(err);
        }
    },
    
    editLocalUser: async (cols) => {
        var query = ['UPDATE users'];
        query.push('SET');

        var set = []

        var keysArray = Object.keys(cols)
        var id = cols.id_user; 
        var info = keysArray.splice(1, keysArray.length - 1);
        
        if (info.length == 0) {
            return false;
        }

        info.forEach(function(key, i) { 
            if (cols[key])
                set.push(key + ' = ' + '\'' + cols[key] + '\'');
        });
        query.push(set.join(', '));

        query.push('WHERE id_user = ' + id);

        query.join(' ');
        let update = query.join(' '); 
 
        const select = `SELECT * FROM users WHERE id_user = '${id}';`;
        
        try {
            await db.query(update, []);
            const result = await db.query(select, []);
            return result[0];
        } catch (err) {
            console.log(err);
        } 

    },
}