const db = require(".");
const { getAllUsers } = require("./userQueries");

module.exports = {
    getNonChecked: async () => {
        let sql = `SELECT * FROM places WHERE checked = false;`;

        try {
            const nonChecked = await db.query(sql);
            return nonChecked;
        } catch (err) {
            throw err;
        }
    },

    getAdminId: async (id_user) => {
        let sql = `SELECT username FROM users WHERE id_user = '${id_user}';`;

        try {
            const select = await db.query(sql);

            if (select.length > 0 && select[0].username == "admin")
                return true;
            else
                return false;

        } catch (err) {
            throw err;
        }
    },

    updateVerified: async (id_place, verified) => {
        let sql;
        if (!verified) {
            sql = `DELETE FROM places WHERE id_place = '${id_place}';`;
        } else {
            sql = `UPDATE places SET verified = true WHERE id_place = '${id_place}';`;
        }

        try {
            await db.query(sql);
        } catch (error) {
            throw error;
        }
    },

    updateChecked: async (id_place) => {
        const update = `UPDATE places SET checked = true WHERE id_place = '${id_place}';`;
        try {
            await db.query(update);
        } catch (err) {
            throw err;
        }
    },

    getAllUsers: async () => {
        const sql = `SELECT * FROM users;`;
        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            throw err;
        }
    },

    getAllEvents: async () => {
        const sql = `SELECT * FROM events;`;
        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            throw err;
        }
    },

    getAllPlaces: async () => {
        const sql = `SELECT * FROM places WHERE checked = true;`;
        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            throw err;
        }
    },

    getAllHangouts: async () => {
        const sql = `SELECT DISTINCT hangouts.*, (SELECT COUNT(ha2.id_hangout)
									   	    FROM hangouts_attendance ha2 
									   	    WHERE ha1.id_hangout = ha2.id_hangout) AS attendance_count 
	                    FROM hangouts NATURAL JOIN hangouts_attendance ha1;`;
        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            throw err;
        }
    },

    deleteUser: async (id_user) => {
        const sql = `DELETE FROM users WHERE id_user = ${id_user} RETURNING id_user;`;
        try {
            const result = await db.query(sql, []);
            if (result.length == 1)
                return true;
            return false;
        } catch (err) {
            throw err;
        }
    },

    deleteEvent: async (id_event) => {
        const sql = `DELETE FROM events WHERE id_event = ${id_event} RETURNING id_event;`;
        try {
            const result = await db.query(sql, []);
            if (result.length == 1)
                return true;
            return false;
        } catch (err) {
            throw err;
        }
    },

    deletePlace: async (id_place) => {
        const sql = `DELETE FROM places WHERE id_place = ${id_place} RETURNING id_place;`;
        try {
            const result = await db.query(sql, []);
            if (result.length == 1)
                return true;
            return false;
        } catch (err) {
            throw err;
        }
    },

    deleteHangout: async (id_hangout) => {
        const sql = `DELETE FROM hangouts WHERE id_hangout = ${id_hangout} RETURNING id_hangout;`;
        try {
            const result = await db.query(sql, []);
            if (result.length == 1)
                return true;
            return false;
        } catch (err) {
            throw err;
        }
    },
}