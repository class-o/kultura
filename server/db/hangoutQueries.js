const db = require(".");

module.exports = {

    getAllHangouts: async () => {
        const sql =
            `SELECT DISTINCT hangouts.*, (SELECT COUNT(ha3.id_hangout)
									   	    FROM hangouts_attendance ha3 
									   	    WHERE ha1.id_hangout = ha3.id_hangout) AS attendance_count 
	            FROM hangouts NATURAL JOIN hangouts_attendance ha1
	            WHERE (SELECT MAX(arrival) 
			            FROM hangouts_attendance ha2 
			            WHERE ha1.id_hangout = ha2.id_hangout) > CURRENT_TIMESTAMP - (3 * interval '1 hour');`;

        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            return false;
        }
    },

    createHangout: async (id_user, start_datetime, long, lat) => {
        const checkSQL = `SELECT DISTINCT hangouts.*, (SELECT COUNT(ha3.id_hangout)
									   	    FROM hangouts_attendance ha3 
									   	    WHERE ha1.id_hangout = ha3.id_hangout) AS attendance_count 
	            FROM hangouts NATURAL JOIN hangouts_attendance ha1
	            WHERE (SELECT MAX(arrival) 
			            FROM hangouts_attendance ha2 
			            WHERE ha1.id_hangout = ha2.id_hangout) > CURRENT_TIMESTAMP - (3 * interval '1 hour')
                    AND Abs(long - ${long}) < 0.005 
                        AND Abs(lat - ${lat}) < 0.005;`;

        try {
            const result = await db.query(checkSQL);
            if (result.length > 0) {
                return false;
            }
        } catch (err) {
            return false;
        }

        const sql = `INSERT INTO hangouts(start_datetime, long, lat)
                        VALUES ('${start_datetime}', '${long}', '${lat}');`
        const selectHangout = `SELECT id_hangout FROM hangouts 
                                WHERE start_datetime = '${start_datetime}' AND long = '${long}' AND lat = '${lat}';`;

        var idHangout;
        try {
            await db.query(sql, []);
            idHangout = await db.query(selectHangout, []);
        } catch (err) {
            return false;
        }
        console.log("tu")
        console.log(id_user)
        const insertUserToHangout = `INSERT INTO hangouts_attendance(id_hangout, id_user, arrival)
                        VALUES ('${idHangout[0].id_hangout.toString()}', '${id_user}', '${start_datetime}');`

        try {
            await db.query(insertUserToHangout, []);
        } catch (err) {
            return false;
        }

        return true;
    },

    addUserToHangout: async (id_hangout, id_user, arrival) => {
        const sql = `INSERT INTO hangouts_attendance(id_hangout, id_user, arrival)
                        VALUES ('${id_hangout}', '${id_user}', '${arrival}');`

        try {
            await db.query(sql, []);
        } catch (err) {
            return false;
        }
        return true;
    }
}

