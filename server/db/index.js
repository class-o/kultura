const {
  Pool
} = require("pg");

//created connection pool with the database
const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "bazepodataka",
  database: "kultura",
  port: 5432
});

//use this method to create queries for the database
//the parameters can be excluded from the method call

module.exports = {
  query: (text, params) => {
    return pool.query(text, params).then((res) => {
      return res.rows; //res.rows to get returned rows
    });
  },
};