const db = require(".");

module.exports = {

    login: async (email, password) => {
        const sql = `SELECT id_place, password FROM places WHERE email = '${email}';`;

        try {
            const place = await db.query(sql);
            if (place.length > 0 && place[0].password == password)
                return place[0];

            return false;
        } catch (err) {
            throw err;
        }
    },

    createPlace: async (name, email, password, lat, long, verified, checked) => {
        const select = `SELECT * FROM places WHERE email = '${email}';`;

        try {
            const places = await db.query(select, []);
            if (places.length != 0)
                return false;

            const insert = `INSERT INTO places (name, email, password, lat, long, verified, checked) VALUES ('${name}', '${email}', '${password}',
            '${long}','${lat}', '${verified}', '${checked}');`

            const place = await db.query(insert);

            return true;
        } catch (err) {
            throw err;
        }
    },

    getPlace: async (id_place) => {
        const sql = `SELECT * FROM places WHERE id_place = '${id_place}';`;

        try {
            const place = await db.query(sql, []);

            if (place.length == 0 || place[0].verified != 1)
                return false;

            return place[0];
        } catch (err) {
            throw err;
        }
    },

    getAllVerified: async () => {
        const sql = `SELECT * FROM place WHERE verified = 1;`;

        try {
            const allPlaces = await db.query(sql);
            return allPlaces;
        } catch (err) {
            throw err;
        }
    },

    editPlace: async (cols) => {

    },

    addEvent: async (title, description, start_datetime, end_datetime, long, lat, id_type, category, attendance, id_place) => {
        let insertEvent = `INSERT INTO events (title, description, start_datetime, end_datetime, long, lat, id_type, id_category, attendance, id_place)
        VALUES ('${title}', '${description}', '${start_datetime}', '${end_datetime}', '${long}','${lat}', '${id_type}', '${category}', '${attendance}', '${id_place}');`

        const selectEvent = `SELECT id_event FROM events WHERE title = '${title}' AND 
        start_datetime = '${start_datetime}' AND long = '${long}' AND lat = '${lat}';`;

        try {
            await db.query(insertEvent, []);
            const result = await db.query(selectEvent, []);
            return result;
        } catch (err) {
            console.log(err);
        }
    },

    getAllPlaces: async () => {
        const sql = `SELECT id_place,name, profile_picture, desc_picture, long, lat FROM places WHERE checked = true;`
        try {
            const all = await db.query(sql);
            if (all.length == 0) {
                return false;
            }
            console.log(all)
            return all;
        } catch (err) {
            throw err;
        }
    }
}