const db = require(".");

module.exports = {

    addEvent: async (title, description, start_datetime, end_datetime, long, lat, id_type, category, id_organizer, attendance) => {
        let insertEvent = `INSERT INTO events (title, description, start_datetime, end_datetime, long, lat, id_type, id_category, id_organizer, attendance)
        VALUES ('${title}', '${description}', '${start_datetime}', '${end_datetime}', '${long}','${lat}', '${id_type}', '${category}', '${id_organizer}', '${attendance}');`

        const selectEvent = `SELECT id_event FROM events WHERE title = '${title}' AND 
        start_datetime = '${start_datetime}' AND long = '${long}' AND lat = '${lat}';`;

        try {
            await db.query(insertEvent, []);
            const result = await db.query(selectEvent, []);
            return result;
        } catch (err) {
            console.log(err);
        }
    },

    editEvent: async (cols) => {
        var query = ['UPDATE events'];
        query.push('SET');

        var set = []

        var keysArray = Object.keys(cols)
        var id_event = cols.id_event;

        const sql = `SELECT * 
                        FROM events 
                            WHERE id_event = '${id_event}';`;
        const result = await db.query(sql);
        const id_organizer = result[0].id_organizer;

        var info = keysArray.splice(1, keysArray.length - 1);

        if (info.length == 0) {
            return false;
        }

        info.forEach(function (key, i) {
            if (cols[key]) {
                if (key == "id_organizer" && !(id_organizer == cols[key])) {
                    return false;
                }
                set.push(key + ' = ' + '\'' + cols[key] + '\'');
            }
        });
        query.push(set.join(', '));

        query.push('WHERE id_event = ' + id_event);

        query.join(' ');
        let update = query.join(' ');

        const select = `SELECT * FROM events WHERE id_event = '${id_event}';`;

        try {
            await db.query(update, []);
            const result = await db.query(select, []);
            return result[0];
        } catch (err) {
            console.log(err);
        }
    },

    getAllEvents: async (start_datetime, end_datetime) => {
        const sql =
            `SELECT * 
                FROM events 
                WHERE start_datetime BETWEEN '${start_datetime}' AND '${end_datetime}'
                    OR start_datetime < '${start_datetime}'
                        AND end_datetime > '${start_datetime}';`;

        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getEventsByType: async (type, start_datetime, end_datetime) => {
        const sql =
            `SELECT * 
                FROM events 
                WHERE id_type = '${type}'
                    AND (start_datetime BETWEEN '${start_datetime}' AND '${end_datetime}'
                        OR start_datetime < '${start_datetime}'
                            AND end_datetime > '${start_datetime}');`;

        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getEventsByTag: async (tag, start_datetime, end_datetime) => {
        const sql =
            `SELECT * 
                FROM events 
                WHERE id_category = '${tag}'
                    AND (start_datetime BETWEEN '${start_datetime}' AND '${end_datetime}'
                        OR start_datetime < '${start_datetime}'
                            AND end_datetime > '${start_datetime}');`;

        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getEventsByOrganizer: async (id_organizer) => {
        const sql = `SELECT * 
                        FROM events 
                            WHERE id_organizer = '${id_organizer}';`;

        try {
            const result = await db.query(sql, []);
            return result;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    getAtenddingCount: async (id_event) => {
        const count = `SELECT count(id_user) 
                        FROM request_status 
                            WHERE id_event = '${id_event}' AND verified = 'true';`;

        try {
            const result = await db.query(count, []);
            return result[0].count;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    addAttendance: async (id_event, id_user) => {
        const count = `SELECT count(id_user) 
                        FROM events_attendance 
                            WHERE id_event = '${id_event}' AND events_attendance.id_user = '${id_user}';`;

        try {
            const result = await db.query(count);
            if (result[0].count > 0) {
                return false;
            }
        } catch (err) {
            throw err;
        }

        const sql = `INSERT INTO events_attendance (id_event, id_user)
                        VALUES ('${id_event}', '${id_user}');`

        try {
            const result = await db.query(sql, []);
        } catch (err) {
            throw err;
        }
        return true;
    },

    getEvent: async (id_event) => {
        const event = `SELECT * 
                        FROM events
                            WHERE id_event = '${id_event}';`;

        try {
            const result = await db.query(event);
            if (result.length == 0) {
                return false;
            }
            return result[0];
        } catch (err) {
            throw err;
        }
    },

    getEventByPlace: async (id_place) => {
        const sql = `SELECT * 
                        FROM events
                            WHERE id_place = '${id_place}';`;

        try {
            const result = await db.query(sql);
            if (result.length == 0) {
                return false;
            }
            return result;
        } catch (err) {
            throw err;
        }
    },

    addVerifiedRequest: async (id_event, id_user) => {

        let sql = `SELECT * 
                            FROM request_status 
                                WHERE id_event = '${id_event}' AND request_status.id_user = '${id_user}';`;
        const select = await db.query(sql);
        if (select.length > 0) {
            return false;
        }

        sql = `INSERT INTO request_status (id_event, id_user, checked, verified) VALUES ('${id_event}', '${id_user}', 'true', 'true');`;

        try {
            await db.query(sql);
        } catch (error) {
            throw (error);
        }


    },


    sendRequest: async (id_event, id_user) => {

        let sql = `SELECT * 
                            FROM request_status 
                                WHERE id_event = '${id_event}' AND request_status.id_user = '${id_user}';`;
        const select = await db.query(sql);
        if (select.length > 0) {
            return false;
        }

        sql = `INSERT INTO request_status (id_event, id_user, checked, verified) VALUES ('${id_event}', '${id_user}', 'false', 'false');`;

        try {
            await db.query(sql);
        } catch (error) {
            throw (error);
        }


    },

    getStatus: async (id_event, id_user) => {
        let sql = `SELECT checked, verified
                    FROM request_status 
                        WHERE request_status.id_event = '${id_event}' AND request_status.id_user = '${id_user}';`;


        let select = await db.query(sql, []);
        if (select.length === 0) {
            return 0;
        }

        if (select[0].verified == true) {
            return 2;
        } else if (select[0].checked == false) {
            return 1;
        }
        return 0;
    },

    getNonChecked: async (id_event) => {
        let sql = `SELECT request_status.id_user, first_name, last_name, picture 
                        FROM request_status INNER JOIN users
                            ON request_status.id_user=users.id_user WHERE checked = false AND id_event = '${id_event}';`;
        let nonchecked = await db.query(sql);

        if (nonchecked.length == 0) {
            return false;
        }
        return nonchecked;
    },

    getVerified: async (id_event) => {
        let sql = `SELECT first_name, last_name, picture 
                        FROM request_status INNER JOIN users
                            ON request_status.id_user=users.id_user WHERE verified = true AND id_event = '${id_event}';`;
        let verified = await db.query(sql);

        if (verified.length == 0) {
            return false;
        }
        return verified;
    },

    verifyRequest: async (id_event, id_user, verified) => {
        let sql;
        if (!verified) {
            sql = `DELETE FROM request_status WHERE id_event = '${id_event}' AND request_status.id_user = '${id_user}';`;
        } else {
            sql = `UPDATE request_status SET verified = 'true' WHERE id_event = '${id_event}' AND request_status.id_user = '${id_user}';`;
        }

        try {
            await db.query(sql);
        } catch (error) {
            throw error;
        }
    },

    updateChecked: async (id_event, id_user) => {
        const update = `UPDATE request_status SET checked = 'true' WHERE id_event = '${id_event}' AND request_status.id_user = '${id_user}';`;
        try {
            await db.query(update);
        } catch (err) {
            throw err;
        }
    }
}