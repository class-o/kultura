const {
    Router
} = require('express');
const express = require('express');
const db = require('../db');
const eventQueries = require('../db/eventQueries');

const eventRouter = express.Router();

eventRouter.post("/add-event", (req, res) => {
    (async () => {
        console.log(req.body);
        let event = await eventQueries.addEvent(req.body.title, req.body.description, req.body.start_datetime, req.body.end_datetime,
            req.body.long, req.body.lat, req.body.id_type, req.body.category, req.body.id_organizer, req.body.attendance)

        res.send(event);
    })();
})

eventRouter.post("/edit/", (req, res) => {
    (async () => {
        const editEvent = await eventQueries.editEvent(req.body);
        if (editEvent) {
            res.status(200).send(editEvent.id_event.toString());
        } else {
            res.sendStatus(400);
        }
    })();
})

eventRouter.get("/organizer/:id", (req, res) => {
    (async () => {
        let events = await eventQueries.getEventsByOrganizer(req.params.id)

        res.send(events)
    })();
})

eventRouter.get("/all", (req, res) => {

    (async () => {
        // checks if tag or category was sent
        let tagChecker = req.query.tag !== undefined;
        let typeChecker = req.query.type !== undefined;

        // checks if start_date was sent - if not, forward current timestamp
        let start_datetime, start_date_checker = req.query.start_date !== undefined;
        // checks if end_date was sent - if not, forward 24 hours after current timestamp
        let end_datetime, end_date_checker = req.query.end_date !== undefined;


        if (!start_date_checker) {
            let d = new Date();
            start_datetime = d.toISOString().split('T')[0] + ' ' + d.toTimeString().split(' ')[0];
        } else {
            start_datetime = req.query.start_date.split('T')[0] + ' 06:00:00';
        }

        if (!end_date_checker) {
            let d = new Date();
            end_datetime = d.toISOString().split('T')[0] + ' ' + d.toTimeString().split(' ')[0];
            const pom = new Date(end_datetime);
            if (req.query.search !== undefined) {
                end_datetime = new Date(pom.setDate(pom.getDate() + 365));
            } else {
                end_datetime = new Date(pom.setDate(pom.getDate() + 7));
            }
        } else {
            end_datetime = req.query.end_date.split('T')[0] + ' 06:00:00';
            const pom = new Date(end_datetime);
            end_datetime = new Date(pom.setDate(pom.getDate() + 1));
        }

        // get the next day
        end_datetime = end_datetime.toISOString().split('T')[0] + ' ' + end_datetime.toTimeString().split(' ')[0];

        // call a specific query (in ../db/eventQueries) based on given query parameters
        let retEvents;
        if (tagChecker) {
            retEvents = await eventQueries.getEventsByTag(req.query.tag, start_datetime, end_datetime);
        } else if (typeChecker) {
            retEvents = await eventQueries.getEventsByType(req.query.type, start_datetime, end_datetime);
        } else {
            retEvents = await eventQueries.getAllEvents(start_datetime, end_datetime);
        }

        res.json(retEvents);
    })();
})

eventRouter.get("/attending_count/:id_event", (req, res) => {
    (async () => {

        let attendig_count = await eventQueries.getAtenddingCount(req.params.id_event);

        res.send(attendig_count);
    })();
})

eventRouter.get("/:id_event", (req, res) => {
    (async () => {

        let event = await eventQueries.getEvent(req.params.id_event);

        if (event == false) {
            res.sendStatus(400);
        } else {
            res.send(event);
        }

    })();
})

eventRouter.post("/attend-event/:id_event/:id_user", (req, res) => {
    (async () => {
        let event = await eventQueries.getEvent(req.params.id_event);

        if (event.id_type == 0) {
            await eventQueries.addVerifiedRequest(req.params.id_event, req.params.id_user);
            res.sendStatus(200);
        } else {
            let request = await eventQueries.sendRequest(req.params.id_event, req.params.id_user);
            if (request == true) {
                res.sendStatus(200);
            } else {
                res.sendStatus(400);
            }
        }
    })();
})

eventRouter.get("/request-status/:id_event/:id_user", (req, res) => {
    (async () => {
        let status = await eventQueries.getStatus(req.params.id_event, req.params.id_user);
        res.status(200).send(status.toString());
    })();
})

eventRouter.get("/non-checked-requests/:id_event", (req, res) => {
    (async () => {
        console.log("nonchecked");
        let nonChecked = await eventQueries.getNonChecked(req.params.id_event);

        if (nonChecked)
            res.status(200).send(nonChecked);
        else
            res.status(400);
    })();
})

eventRouter.get("/verified-requests/:id_event", (req, res) => {
    (async () => {
    
        let verified = await eventQueries.getVerified(req.params.id_event);

        if (verified) {
            res.status(200).send(verified);
        } else {
            res.status(200).send([]);
        }
    })();
})

eventRouter.post("/verify-request/:id_event/:id_user", (req, res) => {
    (async () => {
        console.log(req.body)
        console.log(req.params)
        await eventQueries.updateChecked(req.params.id_event, req.params.id_user);
        await eventQueries.verifyRequest(req.params.id_event, req.params.id_user, req.body.allowed);
        res.sendStatus(200);
    })();
})


eventRouter.get("/event-by-place/:id", (req, res) => {
    (async () => {
        let events = await eventQueries.getEventByPlace(req.params.id);

        if (events) {
            res.status(200).send(events);
        } else {
            res.send(400);
        }
    })
})

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        console.log("here");
    console.log(req.isAuthenticated());
    next()
}

module.exports = eventRouter;