var express = require('express');
const db = require('../db');
var router = express.Router();
const adminQueries = require('../db/adminQueries');

router.get("/all/non-checked", (req, res) => {
    (async () => {
        console.log("tu")
        const nonCheckedPlaces = await adminQueries.getNonChecked();

        if (nonCheckedPlaces.length > 0) {
            res.status(200).send(nonCheckedPlaces);
        } else {
            res.status(401).send("No more non checked places.");
        }
    })();
});

router.post("/update-verified/", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.body.id_user);

        if (admin) {
            await adminQueries.updateChecked(req.body.id_place)
            await adminQueries.updateVerified(req.body.id_place, req.body.verified);
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }
    })();
});

router.get("/users/all/:id_user", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.params.id_user);

        if (admin) {
            const result = await adminQueries.getAllUsers();
            res.status(200).send(result);
        } else {
            res.sendStatus(401);
        }
    })();
});

router.get("/places/all/:id_user", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.params.id_user);

        if (admin) {
            const result = await adminQueries.getAllPlaces();
            res.status(200).send(result);
        } else {
            res.sendStatus(401);
        }
    })();
});


router.get("/events/all/:id_user", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.params.id_user);
        if (admin) {
            const result = await adminQueries.getAllEvents();
            res.status(200).send(result);
        } else {
            res.sendStatus(401);
        }
    })();
});

router.get("/hangouts/all/:id_user", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.params.id_user);
        if (admin) {
            const hangouts = await adminQueries.getAllHangouts();
            res.status(200).send(hangouts);
        } else {
            res.sendStatus(401);
        }
    })();
});

router.post("/users/delete", (req, res) => {
    (async () => {
        console.log(req.body)
        const admin = await adminQueries.getAdminId(req.body.id_user);

        if (admin) {
            const result = await adminQueries.deleteUser(req.body.userID);
            if (result)
                res.status(200).send();
            else
                res.status(400).send();
        } else {
            res.sendStatus(401);
        }
    })();
});

router.post("/events/delete", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.body.id_user);

        if (admin) {
            const result = await adminQueries.deleteEvent(req.body.eventID);
            if (result)
                res.status(200).send();
            else
                res.status(400).send();
        } else {
            res.sendStatus(401);
        }
    })();
});

router.post("/places/delete", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.body.id_user);

        if (admin) {
            const result = await adminQueries.deletePlace(req.body.placeID);
            if (result)
                res.status(200).send();
            else
                res.status(400).send();
        } else {
            res.sendStatus(401);
        }
    })();
});

router.post("/hangouts/delete", (req, res) => {
    (async () => {
        const admin = await adminQueries.getAdminId(req.body.id_user);

        if (admin) {
            const result = await adminQueries.deleteHangout(req.body.hangoutID);
            if (result)
                res.status(200).send();
            else
                res.status(400).send();
        } else {
            res.sendStatus(401);
        }
    })();
});

module.exports = router;