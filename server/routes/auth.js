var express = require('express');
var router = express.Router();
const passport = require('passport');
const fbUser = require("../auth/facebook");
const googleUser = require("../auth/google");
const localUser = require("../auth/local");

router.get('/facebook', passport.authenticate('facebook', {
    scope: ["email", "user_birthday", "user_gender"]
}), function (req, res) {
    console.log("here")
});

router.get('/facebook/callback', passport.authenticate('facebook', {
    successRedirect: "/auth/profile",
    failureRedirect: "/auth/error"
}));

router.get('/google',
    passport.authenticate('google', {
        scope: ['https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/user.birthday.read',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/user.gender.read'
        ]
    }));

router.get('/google/callback', passport.authenticate('google', {
    successRedirect: "/auth/profile",
    failureRedirect: "/auth/error"
}));

router.post("/local", passport.authenticate('local'),
    (req, res) => {
        res.send({
            "id": req.user.id_user,
            "type": req.user.type
        })
    }
);

router.get("/profile", (req, res) => {
    res.redirect(`http://localhost:3000/redirect/${req.user.id_user}`);
});

router.get("/error", (req, res) => {
    res.redirect(`http://localhost:3000/error`);
});

module.exports = router;