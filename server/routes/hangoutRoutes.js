const express = require('express');
const hangoutQueries = require('../db/hangoutQueries');
const hangoutRouter = express.Router();

const moment = require('moment');

hangoutRouter.get("/all", (req, res) => {
    (async () => {
        let hangouts = await hangoutQueries.getAllHangouts();

        res.send(hangouts)
    })();
})

hangoutRouter.post("/create", (req, res) => {
    (async () => {

        console.log("hangouts+create")
        const createHangout = await hangoutQueries.createHangout(req.body.id_user, moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'), req.body.long, req.body.lat);

        if (createHangout == true) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
        }
    })();
})

hangoutRouter.post("/add-user", (req, res) => {
    (async () => {
        const addUser = await hangoutQueries.addUserToHangout(req.body.id_hangout, req.body.id_user, moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'));

        if (addUser == true) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
        }
    })();
})

module.exports = hangoutRouter;