var express = require('express');
const db = require('../db');
const placeQueries = require('../db/placeQueries');
const bcrypt = require("bcrypt");

const router = express.Router();

router.post("/register/", (req, res) => {
    (async () => {
        const hashedPassword = bcrypt.hashSync(req.body.password, 10);
        const nonVerifiedPlace = await placeQueries.createPlace(req.body.name, req.body.email, hashedPassword,
            req.body.lat, req.body.long, req.body.verified, req.body.checked);

        if (nonVerifiedPlace) {
            return res.sendStatus(200);
        } else {
            return res.sendStatus(400);
        }
    })();
});
 
router.get("/get-place/:id_place", (req, res) => {
    (async () => {
        const place = await placeQueries.getPlace(req.params.id_place);

        if (place) {
            res.status(200).send(place);
        } else {
            res.status(400).send("No place with such id.");
        }
    })();
});

router.get("/all-verified", (req, res) => {
    (async () => {
        const places = await placeQueries.getAllVerified();

        if (places) {
            res.status(200).send(places);
        } else {
            res.status(400);
        }
    })();
});

router.post("/edit-place/", (req, res) => {
    (async () => {
        const place = await placeQueries.editPlace(req.body);

        if (place) {
            res.status(200).send(place.id_place.toString());
        } else {
            res.sendStatus(400);
        }
    })
});

router.post("/add-event", (req, res) => {
    (async () => {
        let event = await placeQueries.addEvent(req.body.title, req.body.description, req.body.start_datetime, req.body.end_datetime,
            req.body.long, req.body.lat, req.body.id_type, req.body.category, req.body.attendance, req.body.id_place);

        res.send(event);
    })();
})

router.post("/login", (req, res) => {
    (async () => {
        const hashedPassword = bcrypt.hashSync(req.body.password, 10);
        const login = await placeQueries.login(req.body.email, hashedPassword);

        if (login) {
            res.status(200).send(login.id_place.toString());
        } else {
            res.sendStatus(400);
        }
    })
}) 

router.get("/all", (req, res) => {
    (async () => {
        const result = await placeQueries.getAllPlaces();

        if (result) {
            res.status(200).send(result);
        } else {
            res.status(400).send("No verified places.");
        }
    })();
});

module.exports = router;