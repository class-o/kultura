const passport = require('passport');
const express = require('express');
const bcrypt = require("bcrypt");
const localUser = require("../auth/local");
const userQueries = require('../db/userQueries');
const authQueries = require('../db/authQueries');

const userRouter = express.Router();

userRouter.post("/register/", (req, res) => {
    (async () => {
        const hashedPassword = bcrypt.hashSync(req.body.password, 10);
        const createLocalUser = await authQueries.createLocalUser(req.body.username, hashedPassword,
            req.body.firstName, req.body.lastName, req.body.email)

        return res.status(200).send(createLocalUser.id_user.toString());
    })();
})

userRouter.post("/username-available/", (req, res) => {
    (async () => {
        const usernameAvailable = await userQueries.usernameAvailable(req.query.username);
        if (usernameAvailable)
            return res.sendStatus(200);

        return res.status(400).send('Username already exists.');
    })();
})

userRouter.post("/email-available/", (req, res) => {
    (async () => {
        const emailAvailable = await userQueries.emailAvailable(req.query.email);

        if (emailAvailable)
            return res.sendStatus(200);


        return res.status(400).send('Email already exists.');
    })();
})

userRouter.get("/get-user/:id", (req, res) => {
    (async () => {
        const getUserInfo = await userQueries.getUserInfo(req.params.id);

        res.send(getUserInfo);
    })();
});

userRouter.get("/logout", (req, res) => {
    //req.logout();
    // req.session.destroy();
})

userRouter.post("/edit/", (req, res) => {
    (async () => {
        const editLocalUser = await authQueries.editLocalUser(req.body)
        if (editLocalUser) {
            res.status(200).send(editLocalUser.id_user.toString());
        } else {
            res.sendStatus(400);
        }
    })();
})

userRouter.get("/all", (req, res) => {
    (async () => {
        console.log("here")
        const result = await userQueries.getAllUsers();

        if (result) {
            res.status(200).send(result);
        } else {
            res.status(400).send("No users.");
        }
    })();
});

module.exports = userRouter;