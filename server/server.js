require("dotenv").config();

const express = require('express');
const app = express();
var passport = require('passport');
var session = require('express-session');
const auth = require("./routes/auth");
const userRoutes = require("./routes/userRoutes");
const eventRoutes = require("./routes/eventRoutes");
const placeRoutes = require("./routes/placeRoutes");
const hangoutRoutes = require("./routes/hangoutRoutes");
const adminRoutes = require("./routes/adminRoutes");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload")
const fillDatabase = require("./database");

let cookie_parser = require("cookie-parser")
app.use(cookie_parser("1234"))

const cors = require('cors');
app.use(cors({ origin: 'http://localhost:3000' }));
app.set('view engine', 'ejs');

app.use(fileUpload());

app.use(session({
  secret: 's3cr3t',
  resave: false,
  saveUninitialized: false
}));

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, cb) {
  cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
  cb(null, obj);
});

app.use("/user", userRoutes);
app.use("/events", eventRoutes);
app.use("/place", placeRoutes);
app.use("/hangouts", hangoutRoutes);
app.use("/auth", auth);
app.use("/admin", adminRoutes);
fillDatabase();

const port = process.env.PORT || 5000;

app.listen(port, () => `Server running on port ${port}`);