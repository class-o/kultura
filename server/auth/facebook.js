const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy;
const authQueries = require('../db/authQueries');
const userQueries = require('../db/userQueries');

passport.use(new FacebookStrategy({
  clientID: process.env.FB_CLIENT_ID,
  clientSecret: process.env.FB_CLIENT_SECRET,
  callbackURL: process.env.FB_CALLBACKURL,
  profileFields: ["id", "email", "name", "gender", "birthday", "displayName", "photos"],
  state: true

}, function (accessToken, refreshToken, profile, done) {
  
  (async () => {
    let user;
    let users = await userQueries.checkUsers(profile._json.email)
    if (users.length === 0) {
      var picture = `https://graph.facebook.com/${profile.id}/picture?width=200&height=200&access_token=${accessToken}`
      var birthday = new Date(profile._json.birthday).toDateString();
      user = await authQueries.createUser(profile.username,
        profile.name.givenName, profile.name.familyName, profile._json.email, profile.gender, birthday, picture);
     } else {
      user = users[0];
    } 
    profile.id_user = user.id_user; 
    return done(null, profile);
  })();
}));