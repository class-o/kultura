const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const userQueries = require('../db/userQueries');

passport.use('local', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallBack: true
}, (username, password, cb) => {

    (async () => {
        let rows = await userQueries.getLoginInfo(username);
        if (rows.length === 0) {
            return cb(null, false);
        } else {
            const user = rows[0];
            bcrypt.compare(password, user.password, (err, res) => {
                if (res) {
                    return cb(null, user)
                }
            })
        }
    })();
}));