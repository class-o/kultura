const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const fetch = require("node-fetch");
const authQueries = require('../db/authQueries');
const userQueries = require('../db/userQueries');

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.GO0GLE_CALLBACKURL,
    profileFields: ["birthday", "email", "first_name", "gender", "last_name"]

  },
  function (accessToken, refreshToken, profile, done) {
    (async () => {
      let user;
      let users = await userQueries.checkUsers(profile.emails[0].value);

      const userInfo = `https://people.googleapis.com/v1/people/${profile.id}?personFields=genders,birthdays&key=AIzaSyAGfAMU7EAWYOOiA1OlWdJ3PVONrKJC_6s&access_token=${accessToken}`

      if (users.length === 0) {
        var gender;
        var birthday;
        await fetch(userInfo, {
            method: "Get"
          })
          .then(res => res.json())
          .then((json) => {
            gender = json.genders[0].value;
            var stringBirthday = json.birthdays[1].date.year + "/" + json.birthdays[1].date.month + "/" + json.birthdays[1].date.day;
            birthday = new Date(stringBirthday).toDateString();
          })
          
        user = await authQueries.createUser(profile.username,
          profile.name.givenName, profile.name.familyName, profile.emails[0].value, gender, birthday, profile._json.picture)
      } else {
        user = users[0];
      }
      profile.id_user = user.id_user; 
      return done(null, profile);
    })();
  }
));

module.exports = passport;