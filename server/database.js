const db = require("./db");
const bcrypt = require("bcrypt");

module.exports = async function () {
    const sql = `INSERT INTO users (username, password, first_name, last_name, email, type) VALUES 
    ('${process.env.ADMIN_USERNAME}', '${bcrypt.hashSync(process.env.ADMIN_PASSWORD, 10)}', 'admin', 'admin', 'admin@gmail.com', 1);
    INSERT INTO users (username, password, first_name, last_name, email, type) VALUES 
    ('anaTerovic', '${bcrypt.hashSync("volimdobresifre", 10)}', 'Ana', 'Terovic', 'ana.terovic@gmail.com', 0);
    INSERT INTO users (username, password, first_name, last_name, email, type) VALUES 
    ('josipIvancevic', '${bcrypt.hashSync("ijavolimdobresifre", 10)}', 'Josip', 'Ivančević', 'josip.ivancevic@gmail.com', 0);
    INSERT INTO users (username, password, first_name, last_name, email, type) VALUES 
    ('ivaPezo', '${bcrypt.hashSync("ijaistovolimdobresifre", 10)}', 'Iva', 'Pezo', 'iva.pezo@gmail.com', 0);
    INSERT INTO users (username, password, first_name, last_name, email, type) VALUES 
    ('katarinaGolub', '${bcrypt.hashSync("volimdobresifre", 10)}', 'Katarina', 'Golub', 'katarina.golub@gmail.com', 0);
    INSERT INTO users (username, password, first_name, last_name, email, type) VALUES 
    ('vidJuracic', '${bcrypt.hashSync("sifra", 10)}', 'Vid', 'Juracic', 'vid.juracic@gmail.com', 0);

    INSERT INTO places (email, password, name, long, lat, verified, checked) VALUES 
    ('katran@gmail.com', '${bcrypt.hashSync("sifra", 10)}', 'Katran', 16.001372242384853, 45.80419712189232, true, true);
    INSERT INTO places (email, password, name, long, lat, verified, checked) VALUES 
    ('h20@gmail.com', '${bcrypt.hashSync("sifra", 10)}', 'H20',  15.969513098201784, 45.80441192125785, true, true);
    INSERT INTO places (email, password, name, long, lat, verified, checked) VALUES 
    ('roko@gmail.com', '${bcrypt.hashSync("sifra", 10)}', 'Roko', 15.947787142384186, 45.78384713611071, true, true);
    INSERT INTO places (email, password, name, long, lat, verified, checked) VALUES 
    ('opera@gmail.com', '${bcrypt.hashSync("sifra", 10)}', 'Opera', 15.979184242385104, 45.81216287652286, true, true);
    INSERT INTO places (email, password, name, long, lat, verified, checked) VALUES 
    ('klubklub@gmail.com', '${bcrypt.hashSync("sifra", 10)}', 'Klub klub', 15.981367082855408, 45.807585469433626, true, true);

    
    INSERT INTO hangouts (start_datetime, long, lat) VALUES 
    ('2021-05-29 20:00:00', 45.80194814900458, 15.969343013548158);
    INSERT INTO hangouts (start_datetime, long, lat) VALUES 
    ('2021-05-30 20:00:00', 45.80952541360068, 15.970019640528792);
    INSERT INTO hangouts (start_datetime, long, lat) VALUES 
    ('2021-05-28 20:00:00', 45.780897003002806, 15.91700759313428);

    INSERT INTO events (title, description, start_datetime, end_datetime, long, lat, id_type, id_category, attendance, id_place)
        VALUES ('Katran Music Festival Live - Open Air', 'Katran Open Air', '2021-05-21 20:00:00', '2021-05-22 06:00:00', 45.80419712189232, 16.001372242384853, 0, 0, 100, 1);
    INSERT INTO events (title, description, start_datetime, end_datetime, long, lat, id_type, id_category, attendance, id_place)
        VALUES ('Zlatne godine', '#hramZabave', '2021-05-28 20:00:00', '2021-05-29 06:00:00', 45.80441192125785, 15.969513098201784, 0, 0, 100, 2);
    INSERT INTO events (title, description, start_datetime, end_datetime, long, lat, id_type, id_category, attendance, id_place)
        VALUES ('James Hype', 'Party', '2021-06-04 20:00:00', '2021-06-05 06:00:00',45.81216287652286, 15.979184242385104, 0, 0, 100, 4);

    `
    try {
        const insert = await db.query(sql);
    } catch (err) {

    }
}